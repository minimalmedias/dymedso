<?php /** Template Name: About */
global $post;
$postID =  $post->ID;

get_header(); ?>

<div class="mt-lg-5">
    <?php
    minimal_get_template_part('/templates/pagenav.php', array('nav' => array(
        pll__('the-company') => pll__('The Company'),
        pll__('committed-to-on-going-research') => pll__('Committed to on-going research'),
        pll__('the-inventor') => pll__('The Inventor'),
        pll__('certifications') => pll__('Certifications')
    )));
    ?>
</div>
<section id="<?php echo pll__('the-company'); ?>">
    <div class="container">
        <div class="row justify-content-center align-items-center">
            <div class="col-md-8 text-center">
                <h2 class="mt-md-5 mt-lg-0 mb-md-4">Dymedso</h2>
                <h3 class="mb-4 mb-md-0"><?php echo pll__('Innovation based on a patient’s real-life experience'); ?></h3>
                <div class="my-md-5"><?php echo pll__('Dymedso, a Canadian-based company, is a pioneer in using sound (acoustics) to treat patients with airway clearance diseases. The Company was founded based on the invention of Louis Plante, a cystic fibrosis (CF) patient, along with the collaboration of a medical team from Sherbrooke University in Quebec, Canada. The Company was incorporated as Dymedso in 2002 by Louis Plante and Yvon Robert (its current President). In 2006, following four years of research, Dymedso launched it’s first digitally-controlled acoustic airway clearance device, the Frequencer 1001. In 2010, the Frequencer V2 and V2x were introduced for home and hospital use respectively.'); ?></div>
                <p class="p-lg-5 my-5 mb-5 mb-md-0">
                    <a href="<?php echo get_template_link('page-frequencer.php'); ?>" class="btn btn-primary">
                        <?php echo pll__('Discover the Frequencer<sup>&reg;</sup>'); ?>
                    </a>
                </p>
            </div>
        </div>
    </div>
</section>
<section class="bg-paleblue py-2 py-md-5" id="<?php echo pll__('committed-to-on-going-research'); ?>">
    <div class="container my-5">
        <div class="row justify-content-center align-items-center">
            <div class="col-md-8 text-center">
                <h2 class="mb-md-4"><?php echo pll__('Committed to on-going research'); ?></h2>
                <div class="my-md-5"><?php echo pll__('Dymedso is an innovative company which continues to explore ways to improve its product offering while also conducting research into developing products to treat an even wider range of patients requiring airway clearance. The Company is committed to better patient care and health outcomes and therefore is involved in clinical trials evaluating the use of the Frequencer in a wide range of patient populations (chronic obstructive pulmonary disease – COPD) and settings (hospital and home use).'); ?></div>
            </div>
        </div>
    </div>
</section>
<section class="py-5" id="<?php echo pll__('the-inventor'); ?>">
    <?php
    echo minimal_get_template_part('/templates/image-texte.php', array(
        'image' => responsiveimage(array(
            'url' => outputimage(get_stylesheet_directory_uri() . '/assets/img/about/founder.jpg'),
            'alt' => pll__('The Inventor'),
            'classes' => 'img-fluid'
        )),
        'text' => array(
            'headertitle' => pll__('The Inventor'),
            'subtitle' => pll__('CF Patient and Inventor'),
            'subsubtitle' => pll__('Louis Plante'),
            'caption' => '<p class="mt-3"><a class="btn-link" data-toggle="modal" data-target="#modal_yt" target="_blank">' . pll__('See the video about the award') . '</a></p>',
            'title' => pll__('A simple and unique discovery'),
            'paragraph' => array(
                pll__('It all started with Louis Plante, a 26 year old CF patient who woke up one morning wondering why he had to leave a rock concert a few months earlier because he was coughing to much. He believed that his coughing may have been related to the fact that he was sitting in proximity to a large speaker.'),
                pll__('In 2015, Louis was internationally recognized in Portugal for his innovation.')
            )
        ),
        'layout' => 'text-image'
    ));
    ?>
    <div class="modal fade-scale" id="modal_yt" tabindex="-1" role="dialog"
         aria-labelledby="modal_yt" aria-hidden="true">
        <div class="vertical-alignment-helper">
            <div class="modal-dialog modal-mdlg vertical-align-center" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"
                                aria-label="Close"></button>
                    </div>
                    <div class="modal-body row align-content-center justify-content-center">
                        <iframe class="w-100" width="560" height="315" src="https://www.youtube.com/embed/2R6J6D3vD3Y" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="py-5">
    <div class="container">
        <div class="row justify-content-center align-items-center">
            <div class="col-md-8 text-center">
                <h4 class="h3 centered"><?php echo pll__('First prototype'); ?></h4>
                <h3 class="h2 undercentered"><?php echo pll__('A solution like no other'); ?></h3>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="mx-lg-5 text-center text-md-left">
                    <p><?php echo pll__('Being a skilled electronics technician[...]'); ?></p>
                </div>
            </div>
            <div class="col-md-6">
                <div class="mx-lg-5 text-center text-md-left">
                    <p><?php echo pll__('At the age of 11 Louis lost his twin sister to CF and like many young CF patients, Louis denied his illness for many years. His FEV1 was at 42% when he first tried his low frequency generator.'); ?></p>
                    <p><?php echo pll__('On a regular basis, Louis used to expectorate 5g of mucus at the end of a CPT session. It was now a constant 65g and more with his new device. It would even reach 96g on occasions. That was in 2001.'); ?></p>
                    <p><?php echo pll__('In 2006, Louis has a lung transplanted... but he kept his clearance device close by... just in case... and twice Louis says it prevented him from loosing one of his new transplanted lungs.'); ?></p>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="py-5">
    <?php
    echo minimal_get_template_part('/templates/image-texte.php', array(
        'image' => responsiveimage(array(
            'url' => outputimage(get_stylesheet_directory_uri() . '/assets/img/about/machine.png'),
            'alt' => pll__('A treatment that worked'),
            'classes' => 'img-fluid'
        )),
        'text' => array(
            'subtitle' => pll__('The Frequencer today'),
            'title' => pll__('A treatment that worked'),
            'paragraph' => array(
                pll__("In 2008, Louis' physician scheduled a new operation, this time to remove his new left lung after a scan showed a severe infection on his upper lobe. Two weeks later, Louis returned for the operation and they performed a new scan to update their previous diagnosis... the infection had disappeared!"),
                pll__('Louis told them he had used his frequency device so much during those 2 weeks it must have dried up the infected area... because no pills or antibiotics were used... just low frequency...'),
                pll__('In 2009, same scenario... same lobe... This time, the physician recommended that Louis repeat what he had done the first time...')
            )
        ),
        'layout' => 'text-image'
    ));
    ?>
</section>
<section>
    <div class="container">
        <div class="row justify-content-center align-items-center">
            <div class="col-md-8 text-center">
                <h2 class="mb-4"><?php echo pll__('Louis’ dream of helping others'); ?></h2>
                <div class="my-md-5"><?php echo pll__('Sadly, after years of living with the many challenges associated with CF, Louis died in April 2017. While Louis is no longer with us, his legacy of helping others lives on through his discovery of how sound (acoustics) and his invention (Frequencer) can treat airway clearance issues. Louis’ dream was to make the Frequencer widely available so that patients like himself, could breath easier through the utilization of acoustics. We at Dymedso are committed to making Louis’ dream a reality by making every effort to ensure those suffering from airway clearance difficulties have access to this novel and effective treatment.'); ?></div>
                <p class="p-md-5 my-5 mb-md-0">
                    <a href="<?php echo get_template_link('page-frequencer.php'); ?>" class="btn btn-primary">
                        <?php echo pll__('Discover the Frequencer<sup>&reg;</sup>'); ?>
                    </a>
                </p>
            </div>
        </div>
    </div>
</section>
<section class="bg-gradientblue py-5" id="certifications">
    <div class="container my-5">
        <div class="row justify-content-center align-items-center">
            <div class="col-md-8 text-center">
                <h2 class="text-white"><?php pll__('Certifications'); ?></h2>
            </div>
        </div>
        <div class="row justify-content-center mt-5">
            <?php
            if (have_rows('certifications', trueid($postID))):
                $key = 0;
                $icon = '';
                while (have_rows('certifications', trueid($postID))) : the_row();
                    if(get_sub_field('logo')!=''){
                        $icon = '<div class="col-md-3 icon" style="background-image:url('.get_sub_field('logo').')"></div>';
                    }
                    ?>
                    <div class="col-12 col-lg-auto">
                        <a class="mb-5 mb-lg-0 circlelink" data-toggle="modal" data-target="#modal_<?php echo $key; ?>">
                            <span class="circletext"><span><?php echo get_sub_field('label'); ?></span></span>
                            <?php echo pll__('Read more'); ?>
                        </a>
                        <div class="modal fade-scale" id="modal_<?php echo $key; ?>" tabindex="-1" role="dialog"
                             aria-labelledby="modal_<?php echo $key; ?>" aria-hidden="true">
                            <div class="vertical-alignment-helper">
                                <div class="modal-dialog modal-mdlg vertical-align-center" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"
                                                    aria-label="Close"></button>
                                        </div>
                                        <div class="modal-body row align-content-center justify-content-center">
                                            <?php echo $icon; ?>
                                            <div class="<?php echo ($icon != '') ? 'col-md-9' : 'col-10'; ?>">
                                                <h5><?php echo get_sub_field('label'); ?></h5>
                                                <?php echo get_sub_field('description'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php $key++; endwhile; ?>
            <?php endif; ?>
        </div>
    </div>
</section>

<?php get_footer(); ?>
