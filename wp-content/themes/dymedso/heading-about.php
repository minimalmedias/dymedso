<?php
function set_heading($return)
{
    $heading['background'] = get_stylesheet_directory_uri() . '/assets/img/about/heading.jpg';
    $heading['text'] = '<div class="text-center m-auto about-heading">';
    $heading['text'] .= '<h1>' . get_field('field_5a68f1da6f5e0') . '</h1>';
    $heading['text'] .= '<img alt="Frequencer" src="' . get_stylesheet_directory_uri() . '/assets/img/logo/frequencer_logo_' . pll_current_language('slug') . '.png" class="frequencerlogo">';
    $heading['text'] .= '</div>';
    $heading['align'] = 'justify-content-end';
    $heading['class'] = '';
    return $heading[$return];
}
