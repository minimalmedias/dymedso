<footer id="footer">
    <div class="bg-black">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-xl-auto  mr-auto col1">
                    <a href="<?php echo home_url('/'); ?>" class="navbar-brand">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/logoinversed/dymedso_<?php echo pll_current_language('slug'); ?>.png"
                             class="img-fluid" alt="Dymedso">
                    </a>
                    <p class="slogan"><?php echo pll__('Experience the difference the Frequencer® can make for you.'); ?></p>
                    <p class="mt-5"><a href="<?php echo get_template_link('page-freetrial.php'); ?>"
                                       class="btn btn-reverse"><?php echo pll__('Request a Free Trial'); ?></a>
                    </p>
                </div>
                <div class="col-lg-3 col-xl-auto col2">
                    <?php
                    echo wp_nav_menu(array(
                        'theme_location' => 'footer1',
                        'container' => false,
                        'echo' => false,
                        'fallback_cb' => '__return_false',
                        'items_wrap' => '<ul id="%1$s" class="list-unstyled ml-xl-5 pl-5">%3$s</ul>',
                        'depth' => 2,
                        'walker' => new dymedso_walker_nav_menu()
                    ));
                    ?>
                </div>
                <div class="col-lg-2 col-xl-auto col3">
                    <?php
                    echo wp_nav_menu(array(
                        'theme_location' => 'footer2',
                        'container' => false,
                        'echo' => false,
                        'fallback_cb' => '__return_false',
                        'items_wrap' => '<ul id="%1$s" class="list-unstyled ml-xl-5 pl-5">%3$s</ul>',
                        'depth' => 2,
                        'walker' => new dymedso_walker_nav_menu()
                    ));
                    ?>
                </div>
                <div class="col-lg-3 col-xl-auto col4">
                    <?php
                    echo wp_nav_menu(array(
                        'theme_location' => 'footer3',
                        'container' => false,
                        'echo' => false,
                        'fallback_cb' => '__return_false',
                        'items_wrap' => '<ul id="%1$s" class="list-unstyled ml-xl-5 pl-5">%3$s</ul>',
                        'depth' => 2,
                        'walker' => new dymedso_walker_nav_menu()
                    ));
                    ?>
                    <div class="ml-xl-5 pl-lg-5">
                        <?php minimal_get_template_part('/components/wpml_switcher.php', array()); ?>
                        <div class="social">
                            <a href="https://www.facebook.com/Dymedso" target="_blank" class="icon-facebook"></a>
                            <a href="https://www.linkedin.com/company/846473/" target="_blank"
                               class="icon-linkedin"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="bg-gray subfooter">
        <div class="container">
            <div class="row">
                <div class="col">
                    &copy; Dymedso <?php echo date('Y'); ?>
                    <a href="http://www.minimalmtl.com" class="minimal"
                       target="_blank"> <?php echo (pll_current_language('slug') == 'fr') ? 'Conception par ' : 'Developped by '; ?>
                        Minimal</a>
                </div>
            </div>
        </div>
    </div>
</footer>

</div></div>
<div class="minimaltransition">
    <div class="spin"></div>
</div>

<?php wp_footer(); ?>
</body>
</html>
