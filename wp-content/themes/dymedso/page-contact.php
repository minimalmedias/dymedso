<?php /** Template Name: Contact */
get_header(); ?>
    <section id="contact" class="headerspaced headersized bg-paleblue py-5">
        <div class="container my-md-5">
            <div class="row justify-content-center">
                <div class="col-lg-6 text-center">
                    <h2 class="mt-md-5 mt-lg-0 mb-md-4"><?php echo pll__('Head office'); ?></h2>
                    <h3 class="mb-5"><?php echo pll__('We’re here to help you breath easier'); ?></h3>
                    <div class="card mt-5">
                        <div class="card-body">
                            <h4 class="h3 my-4">Canada</h4>
                            <h5 class="card-title text-center py-3 mb-4 mb-md-5"><?php echo pll__('Dymedso Inc.'); ?></h5>
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-md-6 text-center">
                                        <h6 class="subtitle"><?php echo pll__('Address'); ?></h6>
                                        <p>
                                            <?php echo pll__('2120, 32nd Avenue'); ?><br>
                                            <?php echo pll__('Montreal, Quebec H8T 3H7'); ?><br>
                                            Canada
                                        </p>
                                    </div>
                                    <div class="col-md-6 text-center">
                                        <h6 class="subtitle"><?php echo pll__('Phone'); ?></h6>
                                        <p>
                                            <a style="color:#000" href="tel:15146369959">
                                                <strong>Tel.</strong>1 514 636 9959</a>
                                            <br>
                                            <strong>Fax.</strong> 1 514 631 9959<br>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="row justify-content-center">
                                <div class="col-md-6 text-center pt-md-4">
                                    <?php if (pll_current_language('slug') == 'fr'): ?>
                                        <h6 class="subtitle">Sans frais au Canada et États-Unis</h6>
                                    <?php else: ?>
                                        <h6 class="subtitle">Toll Free in Canada and the United States</h6>
                                    <?php endif; ?>
                                    <a style="color:#000" href="tel:18773963376">
                                        <strong>Tel.</strong> 1 877 396-3376</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="maploader"></section>
    <section id="contactform">
        <?php $formselect = get_query_var('formid'); ?>
        <div class="container text-center p-3">
            <div class="row justify-content-lg-center">
                <div class="col-lg-8 text-center pb-5">
                    <h2 class="my-5"><?php echo pll__('Contact Us'); ?></h2>
                    <?php if (isset($_POST['selectform'])): ?>
                        <input type="hidden" name="selectform" value="<?php echo $_POST['selectform']; ?>">
                    <?php else: ?>
                        <div class="container-fluid">
                            <div class="gform_body">
                                <ul class="gform_fields top_label form_sublabel_below description_below pl-0 row mb-0">
                                    <li class="gfield field_sublabel_below field_description_below gfield_visibility_visible col-lg-6">
                                        <div class="ginput_container ginput_container_select">
                                            <select id="selectform" name="input_1" class="medium gfield_select"
                                                    tabindex="1">
                                                <?php
                                                foreach (array(
                                                             pll__('General inquiry') => pll__('/contact/general-inquiry'),
                                                             pll__('Request a trial') => pll__('/contact/request-a-trial'),
                                                             pll__('Order for disposables') => pll__('/contact/order-for-disposables'),
                                                             pll__('Order a Frequencer') => pll__('/contact/order-a-frequencer')
                                                         ) as $key => $option):
                                                    echo '<option ' . ((strpos($option, $formselect) !== false) ? 'selected' : '') . ' value="' . $option . '">' . $key . '</option>';
                                                endforeach;
                                                ?>
                                            </select>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    <?php endif; ?>
                    <div id="formarea">
                        <?php

                        if (!isset($formselect) or $formselect == '') {
                            $formselect = 'general-inquiry';
                        }

                        $formidd = array(
                            'demande-generale' => '1',
                            'demander-essai' => '3',
                            'commande-jetable' => '4',
                            'commande-frequencer' => '4',
                            'general-inquiry' => '1',
                            'request-a-trial' => '3',
                            'order-for-disposables' => '4',
                            'order-a-frequencer' => '4'
                        );

                        $selectform = get_query_var('selectform');
                        $params = '';
                        if (isset($selectform) and $selectform != '') {
                            $params = 'field_values="selectform=' . $selectform . '"';
                        }

                        echo do_shortcode('[gravityform id=' . $formidd[$formselect] . ' ' . $params . ' title=false description=false ajax=true]');
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--section class="bg-gradientblue">
    <div class="container text-center p-3">
        <div class="row justify-content-lg-center">
            <div class="col-lg-8 text-center">
                <h2 class="text-white my-5"><?php echo pll__('Interested in a Career at Dymedso?'); ?></h2>
                <p class="my-5"><a href="#" class="btn btn-reverse"><?php echo pll__('Apply Today'); ?></a></p>
            </div>
        </div>
    </div>
</section-->
<?php //wp_mail( 'sdumais@minimalmtl.com', 'testt', 'test' ); ?>
<?php get_footer(); ?>