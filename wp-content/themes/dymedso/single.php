<?php /** Template Name: News Detail */
get_header();
$thumbnail = '';
if (have_posts()): while (have_posts()): the_post();
    ?>
    <section id="heading" class="heading"
             style="background-image: url(<?php echo get_the_post_thumbnail_url(get_the_ID(), 'header'); ?>)">
        <div class="container heading"></div>
    </section>
    <article role="article" id="post_<?php the_ID() ?>" class="bg-paleblue py-5">
        <div class="container my-5">
            <header class="row justify-content-center">
                <div class="col-lg-8 text-center">
                    <h4 class="h3"><?php the_time('d-m-Y') ?></h4>
                    <h2><?php the_title() ?></h2>
                </div>
            </header>
            <section class="row justify-content-center">
                <div class="col-lg-8 py-5">
                    <?php the_content() ?>
                </div>
            </section>
        </div>
    </article>
    <?php endwhile; endif; ?>
<?php get_footer(); ?>

