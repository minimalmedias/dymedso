<?php get_header(); ?>

<div class="container" style="height:80vh; padding-top:200px;">
  <div class="row justify-content-center">
    
    <div class="col-sm-8">
      <div id="content" role="main">
        <br>
        <div class="alert alert-warning text-center">
          <h1><i class="glyphicon glyphicon-warning-sign"></i> <?php echo pll__('Error'); ?> 404</h1>
          <p><?php echo pll__('The page you were looking for does not exist.'); ?></p>
        </div>
      </div><!-- /#content -->
    </div>

    
  </div><!-- /.row -->
</div><!-- /.container -->

<?php get_footer(); ?>
