<?php /** Template Name: Healthcare Pros */
global $post;
$postID =  $post->ID;
get_header(); ?>
<section>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-8 text-center blurb p-5 mb-md-5">
                <p><?php echo frequencer(get_field('field_5a69fe6d8fad5')); ?></p>
                <p class="small"><?php echo frequencer(get_field('field_5a69ff25d440c')); ?></p>
            </div>
        </div>
    </div>
</section>
<?php
minimal_get_template_part('/templates/pagenav.php', array('offset'=>250,'nav' => array(
    pll__('proven-effectiveness') => pll__('Proven Effectiveness'),
    pll__('gentle-application') => pll__('Gentle Application'),
    pll__('easy-set-up') => pll__('Easy Set up'),
    pll__('efficient') => pll__('Efficient'),
    pll__('safe') => pll__('Safe'),
    pll__('testimonials') => pll__('Testimonials')
)));
$c = 0;
foreach (array(
             array(
                 'id' => pll__('proven-effectiveness'),
                 'container' => 'bg-paleblue',
                 'subtitle' => pll__('Design to help you'),
                 'title' => pll__('Proven Effectiveness'),
                 'list' => array(
                     pll__('The Frequencer® induced airway clearance in patients with cystic fibrosis equivalent to that of traditional CPT'),
                     pll__('Results of testing showing significant increases in mucus flow rates using the Frequencer® as a component of CPT'),
                     pll__('High degree of patient satisfaction with the Frequencer® versus traditional CP'),
                 ),
                 'button_label' => pll__('Request a Free Trial'),
                 'button_link' => get_template_link('page-freetrial.php'),
                 'image' => get_stylesheet_directory_uri() . '/assets/img/healthcare_professionals/Image_machine_vue_2.png'
             ),
             array(
                 'id' => pll__('gentle-application'),
                 'container' => '',
                 'subtitle' => pll__('Design to help you'),
                 'title' => pll__('Gentle application'),
                 'list' => array(
                     pll__('The Frequencer® uses considerably lower forces (1.9N) than applied during traditional CPT (58N)'),
                     pll__('As a result, it’s a useful treatment option for patients already weakened by their condition'),
                     pll__('The Frequencer® can safely be one of preferred or primary choices by the patient and respiratory therapists'),
                 ),
                 'button_label' => pll__('Request a Free Trial'),
                 'button_link' => get_template_link('page-freetrial.php'),
                 'image' => get_stylesheet_directory_uri() . '/assets/img/healthcare_professionals/image_2.jpg'
             ),
             array(
                 'id' => pll__('easy-set-up'),
                 'container' => '',
                 'subtitle' => pll__('Design to help you'),
                 'title' => pll__('Easy Set up'),
                 'list' => array(
                     pll__('Can be used throughout the hospital (ICU, adult, pediatrics, nursery…) '),
                     pll__('No need to fit or move patient for treatment • Ideal when faced with special patient conditions (obese, G-tube, trauma…)'),
                     pll__('No calibration required'),
                     pll__('Treatment on one side only (front or back)')
                 ),
                 'button_label' => pll__('Request a Free Trial'),
                 'button_link' => get_template_link('page-freetrial.php'),
                 'image' => get_stylesheet_directory_uri() . '/assets/img/healthcare_professionals/Machine_trepied.png'
             ),
             array(
                 'id' => pll__('efficient'),
                 'container' => '',
                 'subtitle' => pll__('Design to help you'),
                 'title' => pll__('Efficient'),
                 'list' => array(
                     pll__('No limit to the number of treatments per day or per patient '),
                     pll__('Assurance of quality treatment every time and for entire treatment duration'),
                     pll__('Four sizes of adapters to optimize treatment on patients of all ages'),
                     pll__('Smallest 1” adapter ideal for pediatric care')
                 ),
                 'button_label' => pll__('Request a Free Trial'),
                 'button_link' => get_template_link('page-freetrial.php'),
                 'image' => get_stylesheet_directory_uri() . '/assets/img/healthcare_professionals/efficient.png'
             ),
             array(
                 'id' => pll__('safe'),
                 'container' => 'bg-paleblue',
                 'subtitle' => pll__('Design to help you'),
                 'title' => pll__('Safe'),
                 'list' => array(
                     pll__('No concern of repetitive injury to caregiver (shoulder, elbow, wrist…)'),
                     pll__('Reduced risk of cross infection with the use of disposable adapters/filters'),
                     pll__('Possible to treat certain areas more often and for longer periods without negatively impacting patient care')
                 ),
                 'button_label' => pll__('Request a Free Trial'),
                 'button_link' => get_template_link('page-freetrial.php'),
                 'image' => get_stylesheet_directory_uri() . '/assets/img/healthcare_professionals/image_3.jpg'
             )
         ) as $im):
    echo '<section class="mb-md-5 pb-5 py-md-5 ' . $im['container'] . '" id="' . $im['id'] . '">';
    minimal_get_template_part('/templates/image-texte.php', array(
        'image' => responsiveimage(array(
            'url' => outputimage($im['image']),
            'alt' => $im['title'],
            'classes' => 'img-fluid max-height-600'
        )),
        'text' => array(
            'subtitle' => $im['subtitle'],
            'title' => $im['title'],
            'list' => $im['list'],
            'button2' => array(
                'label' => $im['button_label'],
                'url' => $im['button_link']
            )
        ),
        'layout' => (++$c % 2) ? 'text2-image' : 'image-text'
    ));
    echo '</section>';
endforeach;
?>
<?php minimal_get_template_part('/templates/testimonials.php', array('classes' => 'spaced-top')); ?>
<?php get_footer(); ?>
