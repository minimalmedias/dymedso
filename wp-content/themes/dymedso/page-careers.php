<?php /** Template Name: Careers */
get_header();

$jobs = array(
    array(
        'title' => pll__('Consultant'),
        'country' => pll__('Montréal'),
        'text' => pll__('Nam dapibus nisl vitae elit fringilla rutrum. Aenean sollicitudin, erat a elementum rutrum, neque sem pretium metus'),
    ),
    array(
        'title' => pll__('Sales'),
        'country' => pll__('USA'),
        'text' => pll__('Nam dapibus nisl vitae elit fringilla rutrum. Aenean sollicitudin, erat a elementum rutrum, neque sem pretium metus'),
    ),
    array(
        'title' => pll__('Marketing Advisor'),
        'country' => pll__('Montréal'),
        'text' => pll__('Nam dapibus nisl vitae elit fringilla rutrum. Aenean sollicitudin, erat a elementum rutrum, neque sem pretium metus'),
    ),
    array(
        'title' => pll__('Consultant'),
        'country' => pll__('Montréal'),
        'text' => pll__('Nam dapibus nisl vitae elit fringilla rutrum. Aenean sollicitudin, erat a elementum rutrum, neque sem pretium metus'),
    )
);
?>
<section>
    <div class="container pb-md-5 my-5">
        <div class="row align-items-center">
            <div class="col text-center py-5">
                <h2><?php echo pll__('Job openings'); ?></h2>
            </div>
        </div>
        <div class="row justify-content-center">
            <?php foreach ($jobs as $job): ?>
                <div class="col-md-3 card borderless noclick">
                    <div class="card-body text-center px-5">
                        <h5><?php echo $job['title']; ?></h5>
                        <h6 class="mb-4">( <?php echo $job['country']; ?> )</h6>
                        <p class="card-text"><?php echo $job['text']; ?></p>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
        <div class="row align-items-center">
            <div class="col text-center py-5">
                <h2><?php echo pll__('Apply at Dymedso'); ?></h2>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="container-fluid">
                    <?php echo do_shortcode('[gravityform id=2 title=false description=false ajax=true]'); ?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php get_footer(); ?>
