<?php get_header(); ?>
<section id="search" class="headerspaced headersized bg-paleblue py-5">
    <div class="container my-5">
        <div class="row justify-content-center">
            <div class="col-lg-6 text-center">
                <h2 class="mt-5 mt-lg-0 mb-md-4"><?php echo pll__('Search Results'); ?></h2>
                <?php get_template_part('loops/content', 'search'); ?>
            </div>
        </div>
    </div>
</section>
<?php get_footer(); ?>
