<?php /** Template Name: Free Trial */
get_header(); ?>
<section id="freetrial" class="headerspaced bg-paleblue py-5">
    <div class="container my-5">
        <div class="row justify-content-center">
            <div class="col-lg-6 text-center">
                <h2 class="mt-5 mt-lg-0 mb-md-4"><?php echo pll__('Request a Free Trial'); ?></h2>
                <h3><?php echo pll__('Ready to see how easy it to use the state-of-the-art Frequencer<sup>TM</sup>? '); ?></h3>
            </div>
        </div>
    </div>
</section>
<section id="contactform">
    <div class="container text-center p-3">
        <div class="row justify-content-lg-center">
            <div class="col-lg-8 text-center pb-5">
                <h4 class="my-5"><?php echo pll__('Simply complete and submit the following form and we will contact you accordingly. Request your free trial today.'); ?></h4>
                <div class="container-fluid">
                    <?php echo do_shortcode('[gravityform id=3 title=false description=false ajax=true]'); ?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php get_footer(); ?>
