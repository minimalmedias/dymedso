var bodyClasses;

///////////////////////EMERGENCE.JS CONFIGS//////////////////////////////////
emergence.init({
    container: window,
    reset: true,
    handheld: true,
    throttle: 250,
    elemCushion: 0.15,
    offsetTop: 0,
    offsetRight: 0,
    offsetBottom: 0,
    offsetLeft: 0,
    callback: function (element, state) {
        if (state === 'visible') {
            console.log('Element is visible.');
        } else if (state === 'reset') {
            console.log('Element is hidden with reset.');
        } else if (state === 'noreset') {
            console.log('Element is hidden with NO reset.');
        }
    }
});

(function ($) {
    "use strict"; // Start of use strict

    /*
    var FadeTransition = Barba.BaseTransition.extend({
        start: function () {
            Promise
                .all([this.newContainerLoading, this.fadeOut()])
                .then(this.fadeIn.bind(this));
        },
        fadeOut: function () {
            return $(this.oldContainer).animate({opacity: 0}).promise();
        },
        fadeIn: function () {
            var _this = this;
            var $el = $(this.newContainer);
            $(this.oldContainer).hide();
            $el.css({
                visibility: 'visible',
                opacity: 0
            });
            $el.animate({opacity: 1}, 400, function () {
                _this.done();
            });
            $('html, body').animate({
                scrollTop: 0
            }, 1000, "easeInOutExpo");

        }
    });
    var Contact = Barba.BaseView.extend({
        namespace: 'contact',
        onEnter: function () {

            if(!$.trim($("#maploader").html()).length){
            //if($("#maploader").checkEmpty()){
                loadMap();
            }
        },
        onEnterCompleted: function () {
            commonFunctions();
            if ($("#formarea").length > 0){
                loadform(1);
                $('#selectform').on('click',function(e){
                    loadform($(this).val());
                });
            }
        }
    });
    Contact.init();
    var Common = Barba.BaseView.extend({
        namespace: 'common',
        onEnterCompleted: function () {
            commonFunctions();
        }
    });
    Common.init();
    var Newsroom = Barba.BaseView.extend({
        namespace: 'newsroom',
        onEnterCompleted: function () {
            commonFunctions();
            loadArticles(0);
        }
    });
    Newsroom.init();
    Barba.Prefetch.init();
    Barba.Pjax.Dom.wrapperId = 'minimalwrapper';
    Barba.Pjax.Dom.containerClass = 'minimalload';
    Barba.Pjax.ignoreClassLink = 'inside';
    Barba.Pjax.originalPreventCheck = Barba.Pjax.preventCheck;
    Barba.Pjax.preventCheck = function (evt, element) {
        if (!Barba.Pjax.originalPreventCheck(evt, element)) {
            return false;
        }
        if (/.pdf/.test(element.href.toLowerCase())) {
            return false;
        }
        if (/#/.test(element.href)) {
            return false;
        }
        return true;
    };
    Barba.Pjax.getTransition = function () {
        return FadeTransition;
    };
    Barba.Dispatcher.on('newPageReady', function(currentStatus, oldStatus, container, newPageRawHTML) {
        eval(container.querySelector("script").innerHTML); //run new js when page loaded
    });
    Barba.Pjax.start();
    */


    /*no barba */
    commonFunctions();
    if (!$.trim($("#maploader").html()).length) {
        loadMap();
    }
    loadArticles(0);
    if ($("#formarea").length > 0) {
        $('#selectform').on('click', function (e) {
            alert($(this).val());
        });
    }

   function commonFunctions() {

        /* $('*[data-region]').on('click', function () {
            var region = $(this).attr('data-region');
            var language = '';
            if (typeof region !== "undefined") {
                setCookie('dymedsoregion', region, 7);
                $('#chooseregion').modal('hide');
                if ($('html').attr('lang') == 'fr') {
                    language = 'fr/';
                }
                if (region == 'canada') {
                    region = '';
                }
                window.location.href = '/' + language + region;
            }
        }); 
        getRegion();*/

        jQuery('.card').on('click', function () {
            if ($(this).find('a').attr('href') === undefined) {
            } else {
                if ($(this).find('a').attr('href').length) {
                    window.location.href = $(this).find('a').attr('href');
                }
            }
        });

        $('body').scrollspy({
            target: '#pagenav',
            offset: 300
        });

        if ($('#pagenav').length) {
            var topoffset = $('#pagenav').offset();
            var fixedheight = $('#siteheader').height();
            $('#pagenav').affix({
                offset: {
                    top: topoffset.top - fixedheight - 20
                }
            });
        }

        $("#pagenav a").click(function (e) {
            e.preventDefault();
            var id = $(this).attr("href");
            var offset = $(id).offset();
            var fixedheight = $('#siteheader').height();
            if ($('.abovepagenav').length) {
                fixedheight = $('.abovepagenav').height();
            }
            $("html, body").animate({
                scrollTop: offset.top - fixedheight - 50
            }, 1000, 'easeInOutExpo');
        });

        // Smooth scrolling using jQuery easing
        $('a.js-scroll-trigger[href*="#"]:not([href="#"])').click(function () {
            if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                if (target.length) {
                    $('html, body').animate({
                        scrollTop: (target.offset().top - 57)
                    }, 1000, "easeInOutExpo");
                    return false;
                }
            }
        });

        // Closes responsive menu when a scroll trigger link is clicked
        $('.js-scroll-trigger').click(function () {
            $('.navbar-collapse').collapse('hide');
        });

    }


    /*function getRegion() {
        var getregion = getCookie('dymedsoregion');
        if (getregion == 'canada') {
            getregion = '';
        }
        if (getregion) {

            $('a[href]').each(function (e) {
                if ($(this).attr('class') == 'regionset') {

                } else {
                    var newurl = $(this).attr('href');
                    if (newurl.indexOf(document.location.origin) !== -1) {

                        var pathname = newurl.replace(document.location.origin, '');
                        var urlparts = pathname.split('/');
                        var firstpart = urlparts[1];
                        var restofit = pathname.replace(firstpart, '');
                        restofit = restofit.replace('/' + getregion + '/', '');
                        restofit = restofit.replace('//', '/');
                        if (restofit.indexOf("uploads/") >= 0) {
                        } else {

                            if (firstpart == 'fr') {
                                newurl = '/' + firstpart + '/' + getregion + '/' + restofit;
                            } else {
                                newurl = '/' + getregion + '/' + firstpart + '/' + restofit;
                            }
                            if (firstpart == getregion) {
                                newurl = '/' + getregion + '/' + restofit;
                            }
                            newurl = newurl.replace('//', '/');

                            $(this).attr('href', document.location.origin + newurl);
                        }

                    }
                }
            });
        } else {
            $('#chooseregion').modal('show');
        }
    } */

    function setCookie(name, value, days) {
        var expires = "";
        if (days) {
            var date = new Date();
            date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
            expires = "; expires=" + date.toUTCString();
        }
        document.cookie = name + "=" + (value || "") + expires + "; path=/";
    }

    function getCookie(name) {
        var nameEQ = name + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') c = c.substring(1, c.length);
            if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
        }
        return null;
    }

    function eraseCookie(name) {
        document.cookie = name + '=; Max-Age=-99999999;';
    }

    function loadform(formid, selectform) {
        jQuery.ajax({
            method: "POST",
            data: {formid: formid, selectform: selectform},
            url: "/wp-content/themes/dymedso/templates/form.php",
            dataType: "html"
        }).success(function (response) {
            jQuery("#formarea").html(response);
        }).error(function (response) {
            console.log(response);
        }).done(function (response) {

        });
    }

    function loadMap() {
        jQuery.ajax({
            method: "POST",
            url: "/wp-content/themes/dymedso/templates/googlemapsinit.php",
            dataType: "html"
        }).success(function (response) {
            jQuery("#maploader").html(response);
        }).error(function (response) {
            console.log(response);
        }).done(function (response) {

        });
    }


})(jQuery); // End of use strict

jQuery.fn.checkEmpty = function () {
    return !$.trim(this.html()).length;
};