jQuery(function ($) {
//Load first batch of articles on page load
    loadArticles(0);
// Load articles by page (see more button)
    $('body').on('click', '.pager', function (event) {
        $("#articleloader").animate({'opacity':0});
        event.preventDefault();
        var page = $(this).attr('href').substr(7); //substring to strip #paged_ and get just number
        loadArticles(page);
    });
});

function loadArticles(selectedpage) {
    if (selectedpage > 0) {
        jQuery('html,body').animate({scrollTop: jQuery('#articleloader').offset().top - jQuery('#siteheader').height() - 20}, 0);
    }
    var firstload = false;
    if (selectedpage == 0) {
        firstload = true;
        selectedpage = 1;
    }
    jQuery.ajax({
        method: "POST",
        data: {
            page: selectedpage,
            firstload: firstload,
            //wpml_lang: $("#wpml_lang").val()
        },
        url: "/wp-content/themes/dymedso/templates/ajaxnews.php",
        dataType: "html"
    }).success(function (response) {
        jQuery("#articleloader").animate({'opacity':0}).html(response);
    }).error(function (response) {

    }).done(function (response) {
        jQuery('.card:not(.noclick)').css('cursor', 'pointer').on('click', function () {
            var link = jQuery(this).find('a').attr('href');
            window.location.href = link;
        });
        jQuery("#articleloader").animate({'opacity':1},function(){
            var headerheight = jQuery('#siteheader').height();
            if (!firstload) {
                if (jQuery('#paged_' + selectedpage).length) {
                    jQuery('html,body').animate({scrollTop: jQuery('#paged_' + selectedpage).offset().top - headerheight}, 800);
                }
            }
        });
    });
}