<?php /** Template Name: Sitemap */
global $post;
$postID =  $post->ID;
get_header(); ?>
<section id="newsroom" class="headerspaced headersized py-5">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-8 text-left">
                <h2 class="mt-5 mt-lg-0 mb-md-4"><?php echo pll__('Sitemap'); ?></h2>
                <?php wp_nav_menu(array('menu'=>'sitemap','walker' => new dymedso_walker_sitemap())); ?>
            </div>
        </div>
    </div>
</section>
<?php get_footer(); ?>
