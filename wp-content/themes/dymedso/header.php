<!DOCTYPE html>
<html class="no-js" lang="<?php echo pll_current_language('slug'); ?>">
<meta charset="utf-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
<?php wp_head(); ?>
</head>
<body <?php echo updatebodyclass(get_body_class(),trueid(get_queried_object_id())); ?> data-spy="scroll" data-target="#pagenav">
<div id="minimalwrapper">
    <div class="minimalload" data-namespace="<?php echo barbanamespace(get_page_template_slug()); ?>">
        <div id="siteheader" class="fixed-top">
            <div id="topbar">
                <div class="container">
                    <div class="row justify-content-between align-items-center">
                        <div class="col-4 mr-auto">
                            <a href="tel:18773963376" class="phone">1 877 396-3376</a>
                        </div>
                        <div class="col-auto text-right">
                            <form id="searchform" class="form-inline" role="search" method="get"
                                  action="<?php
                                  $lang = (pll_current_language('slug')=='fr')?'fr/':'';
                                  //$home = home_url('/'.$lang.str_replace('canada','', $_COOKIE["dymedsoregion"]));
                                  $home = home_url('/');
                                  echo $home;
                                  ?>">
                                <input type="text" id="s" name="s" class="form-control pull-right"
                                       placeholder="<?php echo pll__('Search', 'dymedso'); ?>">
                                <input type="submit" value="" id="searchbtn">
                            </form>
                        </div>
                        <div class="col-auto text-right">
                            <a href="https://www.facebook.com/Dymedso" target="_blank" class="icon-facebook"></a>
                            <a href="https://www.linkedin.com/company/846473/" target="_blank"
                               class="icon-linkedin"></a>
                        </div>
                       <!-- <div class="col-auto regionselector">
                            <ul class="list-inline">
                                
                                foreach (array(
                                             'canada' => (pll_current_language('slug')=='fr')?'AMÉRIQUES':'AMERICAS',
                                             'international' => 'INTERNATIONAL'
                                         ) as $k => $v):
                                    $url = home_url() . '/' . str_replace('canada', '', $k) . '/';
                                    $class = ($_COOKIE["dymedsoregion"] == $k) ? 'class="active"' : '';
                                    echo '<li ' . $class . '>';
                                    echo '<a data-region="' . $k . '" class="regionset" href="' . $url . '"><span>' . $v . '</span></a>';
                                    echo '</li>';
                                endforeach;
                                ?>
                            </ul>
                        </div>-->
                        <div class="col-auto text-right">
                            <ul class="list-inline"><?php pll_the_languages(); ?></ul>
                        </div>

                    </div>
                </div>
            </div>
            <?php
            $region = '';
            if (isset($_COOKIE["dymedsoregion"])) {
                $region = $_COOKIE["dymedsoregion"];
            }
            foreach (pll_the_languages(array('raw' => 1)) as $l):
                if (pll_current_language('slug') != $l['slug']) {
                    $switch = '<a class="mobile_lang" href="' . $l['url'] . '">' . $l['slug'] . '</a>';
                }
            endforeach;
            minimal_get_template_part('/components/headernavigation.php', array(
                'homeurl' => esc_url(home_url('/')),
                'logo' => ' <img src="' . get_stylesheet_directory_uri() . '/assets/img/logo/dymedso_' . pll_current_language('slug') . '.png" class="img-fluid" alt="Dymedso">',
                'id' => 'mainNav',
                'classes' => 'navbar navbar-expand-lg',
                'headerextra' => $switch,
                'menuobject' =>
                    wp_nav_menu(array(
                        'theme_location' => 'mainmenu',
                        'container' => false,
                        'echo' => false,
                        'menu_class' => '',
                        'fallback_cb' => '__return_false',
                        'items_wrap' => '<ul id="%1$s" class="nav navbar-nav ml-auto w-100 justify-content-end">%3$s</ul>',
                        'depth' => 2,
                        'walker' => new dymedso_walker_nav_menu(),
                        'navitem_a_classes' => 'nav-link js-scroll-trigger'
                    )) .
                    wp_nav_menu(array(
                        'theme_location' => 'submenu',
                        'container' => false,
                        'echo' => false,
                        'menu_class' => '',
                        'fallback_cb' => '__return_false',
                        'items_wrap' => '<ul id="%1$s" class="nav navbar-nav ml-auto w-100 justify-content-end">%3$s</ul>',
                        'depth' => 2,
                        'walker' => new dymedso_walker_nav_menu(),
                        'navitem_a_classes' => 'nav-link js-scroll-trigger'
                    ))
            ));
            ?>
        </div>
        <?php
        $template = str_replace(
                'page-',
                'heading-',
                get_page_template_slug(
                        trueid(get_queried_object_id())
                )
        );
        if(file_exists(get_stylesheet_directory().'/'.$template)){ include $template; }
        if (!is_front_page()): if (function_exists('set_heading')):
        ?>
        <section id="heading" class="heading" style="background-image: url(<?php echo set_heading('background'); ?>)">
            <div class="container heading">
                <div class="row <?php echo set_heading('align'); ?>">
                    <div class="<?php echo set_heading('class'); ?>">
                        <?php echo set_heading('text'); ?>
                    </div>
                </div>
            </div>
        </section>
<?php endif;
endif; ?>