<?php /** Template Name: REGION */
$getid = array('fr'=>'region_page_fr','en'=>'region_page_en');
$theid = get_field($getid[pll_current_language('slug')]);
if($theid!='') {
    include get_page_template_slug($theid);
} else {
    $region = $_COOKIE["dymedsoregion"];
    $url = home_url(). '/' .str_replace('canada','',$region).'/'
        .str_replace('en','',pll_current_language('slug'));
    //wp_redirect( $url );
    echo $url;
    exit;
}
?>
