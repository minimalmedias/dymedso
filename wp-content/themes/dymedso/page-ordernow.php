<?php /** Template Name: Order Now */
global $post;
$postID = $post->ID;
get_header(); ?>
<section id="ordernow" class="headerspaced bg-paleblue py-5">
    <div class="container my-5">
        <div class="row justify-content-center">
            <div class="col-lg-6 text-center">
                <h2 class="mt-5 mt-lg-0 mb-md-4"><?php echo pll__('Order Now'); ?></h2>
                <h3><?php echo pll__('Thank you for joining our ever-growing list of satisfied patients from the world. We encourage you to fill out the form below to order from us again.'); ?></h3>
            </div>
        </div>
    </div>
</section>
<section id="contactform">
    <div class="container text-center p-3">
        <div class="row justify-content-lg-center">
            <div class="col-lg-8 text-center pb-5">
                <h4 class="mt-5"><?php echo pll__('Simply complete and submit the following form.'); ?></h4>
                <p class="mb-5"><?php echo pll__('* For returning customers only '); ?></p>
                <div class="container-fluid">
                    <?php echo do_shortcode('[gravityform id=4 title=false description=false ajax=true]'); ?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php get_footer(); ?>
