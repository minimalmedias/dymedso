<div class="row justify-content-center">
    <div class="col-lg-5 text-center">
        <div class="p-lg-5 m-auto">
            <?php echo responsiveimage(array(
                'url' => outputimage(get_stylesheet_directory_uri() . '/assets/img/healthcare_institutions/Demographics.png'),
                'alt' => pll__(''),
                'classes' => 'img-fluid'
            )) ?>
        </div>
    </div>
    <div class="col-lg-5 text-center">
        <div class="p-lg-5 m-auto">
            <?php echo responsiveimage(array(
                'url' => outputimage(get_stylesheet_directory_uri() . '/assets/img/healthcare_institutions/simplicity.png'),
                'alt' => pll__(''),
                'classes' => 'img-fluid'
            )) ?>
        </div>
    </div>
</div>
<div class="row mt-5 mt-md-0 justify-content-center">
    <div class="col-md-8 text-center">
        <p class="medium"><?php echo pll__('Using a Baylor University standardised questionnaire, patients assess their currently used clearance technique versus the Frequencer<sup>&reg;</sup> over a 2-week trial period.'); ?></p>
    </div>
</div>