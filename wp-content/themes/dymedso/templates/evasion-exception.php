<?php
minimal_get_template_part('/templates/heading.php', array(
    'nav' => true,
    'caption' => get_field($this->page . '-header_blurb'),
    'subcaption' => $this->subcaption
));
?>

<?php minimal_get_template_part('/templates/description.php', array('description' => get_field($this->page . '-blurb_blurb'))); ?>

    <section class="reveal">
        <?php
        #Text + Image
        minimal_get_template_part('/templates/image-texte.php', array(
            'image' => array(
                'url' => standardimage(get_field($this->page . '-bloc_image')),
                'alt' => get_field($this->page . '-bloc_label'),
                'classes' => 'img-fluid my-md-5'
            ),
            'text' => array(
                'subtitle' => get_field($this->page . '-bloc_title'),
                'classes' => 'text-center',
                'text' => get_field($this->page . '-bloc_text'),
            ),
            'overlap' => true,
            'layout' => 'text-image'
        ));
        ?>
    </section>

<?php minimal_get_template_part('/templates/grille.php', array('filtresections' => $this->filtresections)); ?>

<?php minimal_get_template_part('/templates/avantages.php', array()); ?>

<?php minimal_get_template_part('/templates/certificat.php', array()); ?>

<?php minimal_get_template_part('/templates/newsletter.php', array()); ?>