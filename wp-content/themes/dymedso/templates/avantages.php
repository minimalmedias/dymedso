<section id="lesavantages" class="reveal <?php echo(isset($this->classes))?$this->classes:''; ?>">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-center">
                <h5><?php _e('Voyages VP', 'wpml_theme'); ?></h5>
                <h2><?php _e('Les avantages', 'wpml_theme'); ?></h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 text-center">
                <div class="icon-champagne"></div>
                <h3 class="h5"><?php _e('Un accompagnement premium', 'wpml_theme'); ?></h3>
                <p class="px-md-5"><?php _e('De notre première conversation jusqu’au retour, nous prenons soin de gérer les détails de votre voyage pour qu’ils répondent, avec attention et anticipation, à chacun de vos besoins et de vos désirs.', 'wpml_theme'); ?></p>
            </div>
            <div class="col-md-6 text-center">
                <div class="icon-gate"></div>
                <h3 class="h5"><?php _e('Exclusivité et primeur', 'wpml_theme'); ?></h3>
                <p class="px-md-5"><?php _e('Partir avec nous, c’est avoir accès à une foule d’avantages. Vols, hôtels, croisières… Partir n’aura jamais été aussi exclusif. Bénéficiez d’offres exceptionnelles et de propositions uniques grâce à notre vaste réseau qui nous permet de bénéficier de tarifs avantageux.', 'wpml_theme'); ?></p>
            </div>
        </div>
    </div>
</section>