<section id="vosescales" class="spaced-top">
    <div class="container">
        <div class="row">
            <div class="col"><h2><?php _e('Vos escales', 'wpml_theme'); ?></h2></div>
        </div>
    </div>
    <div class="mapzone">
        <?php
        if (!empty($this->map_select)) {
            foreach ($this->map_select as $map) {
                if ($map['acf_fc_layout'] == 'google_map') {
                    echo $map['map'];
                }
                if ($map['acf_fc_layout'] == 'image_map') {
                    echo '<img src="' . $map['image_map_f']['sizes']['large'] . '" alt="" class="img-fluid" class="mapimage">';
                }
            }
        }
        ?>
    </div>
    <div class="container spaced-top">
        <div class="row">
            <div class="col">
                <?php /*
                <div class="timeline">
                    <div><h2>Vos escales en détail</h2></div>
                    <?php
                    //Escales
                    $escales = array(
                        array(
                            "Orlando (Lieu de départ)",
                            "Les voyageurs séjourneront d’abord à l’hôtel Ritz Carlton Grande Lake."
                        ),
                        array(
                            "Cusco et Machu Picchu",
                            "Les voyageurs accèdent au site inca par train privé. Hébergement à l’hôtel Belmont Palacio Nazarenas."
                        ),
                        array(
                            "Île de Pâques",
                            "Vous verrez entre autre le village Orongo, lieu de cérémonie Moaïs. Hébergement au Hangaroa Eco Village and spa."
                        ),
                        array(
                            "Apia, île de Samoa",
                            "Vous apprécierez les fonds marins des Samoa qui sont d'une rare beauté. Hébergement au Sheraton Samoa Aggie Grey."
                        )
                    );
                    foreach ($escales as $escale): ?>
                        <div>
                            <h3><?php echo $escale[0]; ?></h3>
                            <p><?php echo $escale[1]; ?></p>
                        </div>
                    <?php endforeach; ?>
                </div>
                */ ?>
                <div class="mobile-centered">
                    <a href="<?php echo get_template_link('page-contact.php'); ?>" class="btn btn-secondary <?php /* spaced-top */ ?>"><span
                                class="icon bell"></span>
                        <?php _e('Demande d\'information', 'wpml_theme'); ?></a>
                </div>
            </div>
        </div>
    </div>
</section>