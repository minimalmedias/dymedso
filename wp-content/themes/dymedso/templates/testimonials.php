<section class="bg-gradientblue" id="testimonials">
    <div class="container text-center p-3">
        <div class="row justify-content-lg-center">
            <div class="col-lg-8">
                <h2 class="m-5 text-white"><?php echo pll__('Testimonials'); ?></h2>
                <?php
                $items=array();
                $wp_query = new WP_Query(array('post_status' => 'publish','post_type'=>'Testimonials','posts_per_page'=>'-1'));
                if ($wp_query->have_posts()):
                    while ($wp_query->have_posts()) : $wp_query->the_post();
                        $id = get_the_ID();
                        $items[$id]['author'] = frequencer(get_field('field_5a68e1d3c7563'));
                        $items[$id]['text'] = frequencer(get_field('field_5a68e1a4c7562'));
                    endwhile;
                endif;
                minimal_get_template_part(
                        '/components/carousel.php',
                        array('id' => 'testimonialslides',
                            'interval' => 5000,
                            'template' => '/templates/testimonial.php',
                            'indicators' => 1,
                            'items' => $items)
                );
                ?>
                <p class="pt-5">&nbsp;</p>
            </div>
        </div>
    </div>
</section>