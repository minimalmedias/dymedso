<section id="newsletter" class="<?php echo(isset($this->classes))?$this->classes:''; ?>">
    <div class="col text-center">
        <div class="icon"></div>
        <h4><?php _e('Pour être informé en primeur de nos dernières offres.', 'wpml_theme'); ?></h4>
        <form class="form form-inline justify-content-center">
            <div class="form-group"><input type="text" class="form-control"></div>
            <div class="form-group"><input type="submit" value="envoyer" class="btn btn-secondary"></div>
        </form>
    </div>
    <div class="labelcontainer"><h5 class="verticallabel"><?php _e('Infolettre Voyages VP', 'wpml_theme'); ?></h5></div>
</section>