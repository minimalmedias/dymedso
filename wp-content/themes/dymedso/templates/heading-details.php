<div class="fiche-overlay">
    <div class="container">
        <div class="row align-items-end justify-content-center">
            <div class="col-10">
                <div class="row align-items-end">
                    <div class="<?php echo $this->classes; ?> boxed">
                        <?php echo $this->header; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php if(!empty($this->details)){ ?>
<section id="fiche-details">
    <div class="container">
        <div class="row">
            <div class="col-md-1">
                <div class="desktop verticallabel"><h5><?php _e('Détails', 'wpml_theme'); ?></h5></div>
            </div>
            <div class="col-md-10">
                <div class="container-fluid detailcontainer">
                    <div class="row">
                        <?php
                        foreach ($this->details as $detail) {
                            if (!is_array($detail['text'])) {
                                $first = $detail['text'];
                                $detail['text'] = array($first);
                            }
                            ?>
                            <div class="col <?php echo (isset($this->subclasses))?$this->subclasses:''; ?>">
                                <div class="label">
                                    <div class="<?php echo $detail['icon']; ?>"></div>
                                    <h5><?php echo $detail['title']; ?></h5>
                                </div>
                                <?php foreach ($detail['text'] as $p) {
                                    echo '<p>';
                                    echo (isset($detail['url']))?'<a href="'.$detail['url'].'" target="_blank">'.$p.'</a>':$p;
                                    echo '</p>';
                                } ?>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php } ?>