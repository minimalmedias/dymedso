<?php
$texttitle = (isset($this->text['title'])) ? (($this->text['title'] != '') ? '<h3 class="h2">' . $this->text['title'] . '</h3>' : '') : '';
$subtitle = (isset($this->text['subtitle'])) ? (($this->text['subtitle'] != '') ? '<h4 class="h3">' . $this->text['subtitle'] . '</h4>' : '') : '';
$subsubtitle = (isset($this->text['subsubtitle'])) ? (($this->text['subsubtitle'] != '') ? '<h4 class="subsubtitle">' . $this->text['subsubtitle'] . '</h4>' : '') : '';
$caption = (isset($this->text['caption'])) ? (($this->text['caption'] != '') ? $this->text['caption'] : '') : '';
$headertitle = '';
if (isset($this->text['headertitle'])) {
    if($this->text['headertitle'] != ''){
        $headertitle .= '<div class="row">';
        $headertitle .= '<div class="col-12 text-center">';
        $headertitle .= '<h3 class="h2 centered mb-5">' . $this->text['headertitle'] . '</h3>';
        $headertitle .= '</div>';
        $headertitle .= '</div>';
    }
}
$paragraph = '';

//hack thropugh object, optimize later. used to display image block only if image exists
$thisimage = '';
if (isset($this->image)) {
    if ($this->image != '') {
        $thisimage = $this->image;
    }

}
if (isset($this->text['paragraph'])) {
    foreach ($this->text['paragraph'] as $p) {
        $paragraph .= '<p>' . $p . '</p>';
    }
}
elseif (isset($this->text['list'])) {
    $paragraph .= '<ul class="list-unstyled checklist">';
    foreach ($this->text['list'] as $p) {
        $paragraph .= '<li><div>' . $p . '</div></li>';
    }
    $paragraph .= '</ul>';
}
elseif (isset($this->text['text'])) {
    if ($this->text['text'] != '') {
        $paragraph = '<p>' . $this->text['text'] . '</p>';
    }
}
$button = '';
if (isset($this->text['button']) and $this->text['button']['label'] != '') {
    $buttonclasses = (isset($this->text['button']['classes'])) ? $this->text['button']['classes'] : 'btn-primary';
    $button .= '<a href="' . $this->text['button']['url'] . '" class="btn ' . $buttonclasses . '">';
    $button .= $this->text['button']['label'];
    $button .= '</a>';
}
$button2 = '';
if (isset($this->text['button2']) and $this->text['button2']['label'] != '') {
    $buttonclasses = (isset($this->text['button2']['classes'])) ? $this->text['button2']['classes'] : 'btn-primary';
    $button2 .= '<div class="row mt-md-5"><div class="col-12 text-center">';
    $button2 .= '<a href="' . $this->text['button2']['url'] . '" class="m-auto btn ' . $buttonclasses . '">';
    $button2 .= $this->text['button2']['label'];
    $button2 .= '</a>';
    $button2 .= '</div></div>';
}
$textcellcontent = $subtitle . $subsubtitle . $texttitle . $paragraph . $button;
$containerclasses = 'layout-' . $this->layout;
$imagecellclasses = 'col-lg-6 col-xxl-5';
$textcellclasses = 'col-lg-6 col-xxl-5';
$subdivclasses = (isset($this->text['classes'])) ? $this->text['classes'] : '';

if ($this->layout == 'image-text') {
    // IMAGE + TEXT
    $imagecellclasses = $imagecellclasses . ' text-lg-right';
    $subdivclasses = $subdivclasses . ' text-lg-left mr-xl-5';
}
if ($this->layout == 'text-image') {
    // TEXT + IMAGE
    $imagecellclasses = $imagecellclasses . ' text-lg-left';
    $subdivclasses = $subdivclasses . ' text-lg-right ml-xl-5';
}
if ($this->layout == 'text2-image') {
    // TEXT + IMAGE
    $imagecellclasses = $imagecellclasses . ' text-lg-left';
    $subdivclasses = $subdivclasses . ' text-lg-left ml-xl-5';
}

$fullstring = $headertitle.$texttitle . $subtitle.$subsubtitle;

if ($fullstring != '') {
    $text = '<div class="text-cell ' . $textcellclasses . '"><div class="' . $subdivclasses . '">' . $textcellcontent . '</div></div>';
    $image = ($thisimage != '') ? '<div class="image-cell ' . $imagecellclasses . '">' . $thisimage . $caption . '</div>' : '';
    ?>
    <div class="container mb-md-4 <?php echo $containerclasses; ?> reveal">
        <?php echo $headertitle; ?>
        <div class="row justify-content-lg-center align-items-start">
            <?php echo ($this->layout == 'image-text') ? $image . $text : $text . $image; ?>
        </div>
        <?php echo $button2; ?>
    </div>
<?php } ?>