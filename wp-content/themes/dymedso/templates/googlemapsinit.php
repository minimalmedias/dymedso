<script>
    function initMap() {
        //https://www.google.ca/maps/place/Dymedso+Inc./@45.4640312,-73.7128011,17z/data=!3m1!4b1!4m5!3m4!1s0x4cc917c98a526c1f:0x88f7ead1cfa4cc91!8m2!3d45.4640312!4d-73.7106124
        var myLatLng = {lat: 45.4640312, lng: -73.7128011};
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 15,
            center: myLatLng,
            styles: [
                {
                    "featureType": "administrative",
                    "elementType": "labels.text.fill",
                    "stylers": [
                        {
                            "color": "#444444"
                        }
                    ]
                },
                {
                    "featureType": "landscape",
                    "elementType": "all",
                    "stylers": [
                        {
                            "color": "#f2f2f2"
                        }
                    ]
                },
                {
                    "featureType": "poi",
                    "elementType": "all",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "road",
                    "elementType": "all",
                    "stylers": [
                        {
                            "saturation": -100
                        },
                        {
                            "lightness": 45
                        }
                    ]
                },
                {
                    "featureType": "road.highway",
                    "elementType": "all",
                    "stylers": [
                        {
                            "visibility": "simplified"
                        }
                    ]
                },
                {
                    "featureType": "road.arterial",
                    "elementType": "labels.icon",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "transit",
                    "elementType": "all",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "water",
                    "elementType": "all",
                    "stylers": [
                        {
                            "color": "#46bcec"
                        },
                        {
                            "visibility": "on"
                        }
                    ]
                }
            ]
            
        });
        var marker = new google.maps.Marker({
            position: myLatLng,
            map: map,
            title: ''
        });
    }
</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC1OT2BRbqMcHZS-YB8-PLxkVAr0-eLL-Y&callback=initMap"></script>
<section id="map"></section>