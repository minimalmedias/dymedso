



                <div class="vertical-alignment-helper">
                    <div class="modal-dialog modal-mdlg vertical-align-center" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"
                                        aria-label="Close"></button>
                            </div>
                            <div class="modal-body row align-content-center justify-content-center">
                                <?php echo $icon; ?>
                                <div class="<?php echo ($icon != '') ? 'col-md-9' : 'col-10'; ?>">
                                    <h5><?php echo get_sub_field('field_5a68f37f10fe9'); ?></h5>
                                    <?php echo get_sub_field('field_5a68f489de757'); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

