<section id="certificat" class="reveal <?php echo(isset($this->classes))?$this->classes:''; ?>">
    <div class="container-fluid">
        <div class="row align-items-center">
            <div class="col-md-7 col-lg-5 text-center certificatcard">
                <div class="shadowcard navbar-brand mx-auto"><span class="sr-only"><?php _e('Voyages VP', 'wpml_theme'); ?></span></div>
            </div>
            <div class="col-md-4 col-lg-7 certificattext">
                <div>
                    <p><?php _e('Le certificat-cadeau VOYAGE est la solution toute trouvée pour faire plaisir à vos proches tout au long de l\'année et pour tous vos cadeaux de dernière minute.', 'wpml_theme'); ?></p>
                    <a href="<?php echo get_template_link('page-chequecadeau.php'); ?>" class="btn btn-primary"><?php _e('Tous les détails', 'wpml_theme'); ?></a>
                </div>
            </div>
        </div>
    </div>
</section>