<div class="pagenav-container">
    <nav id="pagenav" class="justify-content-center">
        <div class="wrapper">
            <button
                    class="btn btn-nav collapsed"
                    type="button"
                    data-toggle="collapse"
                    data-target="#pagenavmenu"
                    aria-expanded="false"
                    aria-controls="pagenavmenu"><?php echo pll__('Select content'); ?></button>
            <div class="nav" id="pagenavmenu">
                <?php foreach ($this->nav as $key => $label): ?>
                    <a class="nav-link" href="#<?php echo $key; ?>"><?php echo $label; ?></a>
                <?php endforeach; ?>
            </div>
        </div>
    </nav>
</div>