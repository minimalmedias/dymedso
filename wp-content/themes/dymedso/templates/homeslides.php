<?php if ($this->id == 1): ?>
    <section class="heading bg-gradientblue">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-auto backgroundimage">
                    <h2 class="massive"><?php echo pll__('Breathe'); ?></h2>
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/home/frequencer.png"
                         alt="Breathe" class="machine">
                    <img alt="<?php echo pll__('Breathe'); ?>" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/logo/frequencer.png"
                         class="frequencerlogo">
                    <p><?php echo  pll__('The Frequencer<sup>®</sup> is designed to help those suffering from chronic respiratory diseases to take more control of their health an lives.'); ?></p>
                    <p><a href="<?php echo get_template_link('page-frequencer.php'); ?>" class="btn btn-reverse"><?php echo pll__('Learn more'); ?></a></p>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>
<?php if ($this->id == 2):
    $heading['background'] = get_stylesheet_directory_uri() . '/assets/img/about/heading.jpg';
    $heading['text'] = '<div class="text-center text-md-left m-auto about-heading">';
    $heading['text'] .= '<h1 class="mt-5 mt-md-0">'. pll__('Helping you breath easier').'</h1>';
    $heading['text'] .= '<img src="' . get_stylesheet_directory_uri() . '/assets/img/logo/frequencer_logo_'.pll_current_language('slug').'.png" alt="Frequencer" class="frequencerlogo">';
    $heading['text'] .= '<p><a href="'.get_template_link('page-frequencer.php').'" class="btn btn-primary mt-4">'.pll__('Learn more').'</a></p>';
    $heading['text'] .= '</div>';
    $heading['align'] = 'justify-content-end';
    $heading['class'] = 'text-center text-md-left';
    ?>
    <section class="heading" style="background-image: url(<?php echo $heading['background']; ?>)">
        <div class="container heading">
            <div class="row <?php echo $heading['align']; ?>">
                <div class="<?php echo $heading['class']; ?>">
                    <?php echo $heading['text']; ?>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>
<?php if ($this->id == 3):
    $heading['background'] = get_stylesheet_directory_uri() . '/assets/img/home/Images_entete_3.jpg';
    $heading['text'] = '<div class="text-center text-md-left m-auto about-heading">';
    $heading['text'] .= '<h1 class="mt-5 mt-md-0">'. pll__('Helping you breath easier').'</h1>';
    $heading['text'] .= '<img src="' . get_stylesheet_directory_uri() . '/assets/img/logo/frequencer_logo_'.pll_current_language('slug').'.png" alt="Frequencer" class="frequencerlogo">';
    $heading['text'] .= '<p><a href="'.get_template_link('page-frequencer.php').'" class="btn btn-primary mt-4">'.pll__('Learn more').'</a></p>';
    $heading['text'] .= '</div>';
    $heading['align'] = 'justify-content-end';
    $heading['class'] = 'text-center text-md-left';
    ?>
    <section class="heading" style="background-image: url(<?php echo $heading['background']; ?>)">
        <div class="container heading">
            <div class="row <?php echo $heading['align']; ?>">
                <div class="<?php echo $heading['class']; ?>">
                    <?php echo $heading['text']; ?>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>
