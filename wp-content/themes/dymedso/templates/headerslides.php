<div class="slidebackground backgroundfadein" <?php if (isset($this->img)){ ?>style="background-image:url(<?php echo $this->img; ?>)"<?php } ?>>
    <img src="<?php echo $this->img; ?>" class="imagepreloader">
    <div class="carousel-caption container-fluid">
        <div class="row align-items-center justify-content-center">
            <div class="col-8">
                <?php if (isset($this->subcaption)) { ?>
                    <h5><?php echo $this->subcaption; ?></h5>
                <?php } ?>
                <?php if (isset($this->caption)) { ?>
                    <h1><?php echo $this->caption; ?></h1>
                <?php } ?>
                <?php if (isset($this->button)) { ?>
                    <a class="btn btn-primary"
                       href="<?php echo $this->button['url']; ?>"><?php echo $this->button['label']; ?></a>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
