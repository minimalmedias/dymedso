<?php
require_once("../../../../wp-load.php");
$items_per_load = 2;
$wp_query = new WP_Query(array('post_status' => 'publish'));
if ($wp_query->have_posts()):
    $i = 0;
    while ($wp_query->have_posts()) : $wp_query->the_post();
        $id = get_the_ID();
        $thumbnail = get_the_post_thumbnail_url($id, 'newsthumbnail');
        $query_result[$i]['thumbnail'] = responsiveimage(array(
            'url' => outputimage($thumbnail),
            'alt' => get_the_title(),
            'classes' => 'card-img-top'
        ));
        $query_result[$i]['date'] = get_the_time('d-m-Y');
        $query_result[$i]['title'] = get_the_title($id);
        $query_result[$i]['excerpt'] = get_the_excerpt($id);
        $query_result[$i]['permalink'] = get_permalink();
        $i++;
    endwhile;
endif;
$chunks = array_chunk($query_result, $items_per_load);
$totalpages = count($chunks);
$firstload = $_POST['firstload'];
$nextpage = ($_POST['page'] < $totalpages) ? $_POST['page'] + 1 : false;
$cumulatedchunks = array();
foreach ($chunks as $index => $chunk):
    if ($index < $_POST['page']): $cumulatedchunks[] = $chunk; endif;
endforeach;
$i = 0;
foreach ($cumulatedchunks as $index => $newschunk):
    $ii = 0;
    foreach ($newschunk as $news):
        ?>
        <div class="col-md-4 card">
            <?php echo $news['thumbnail']; ?>
            <div class="card-body text-center" <?php echo ($ii == 0) ? ' id="paged_' . ($index + 1) . '"' : ''; ?>>
                <h6><?php echo $news['date']; ?></h6>
                <h5 class="card-title text-center fiche"><?php echo $news['title']; ?></h5>
                <p class="card-text"><?php echo $news['excerpt']; ?></p>
            </div>
            <div class="card-footer text-right">
                <a href="<?php echo $news['permalink']; ?>"
                   class="btn-link"><?php echo pll__('Read more'); ?></a>
            </div>
        </div>
        <?php
        $ii++;
    endforeach;
    $i++;
endforeach;
if (isset($nextpage)) {
    if ($nextpage != false) {
        $ajaxbutton['nextpage'] = $nextpage;
    }
}
if (isset($items_per_load)) {
    $ajaxbutton['items_per_load'] = $items_per_load;
}

if (isset($postquery)) {
    if ($postquery != '') {
        $data['query'] = $postquery;
    }
}


if (isset($nextpage) and $nextpage!='' and isset($items_per_load)):
    $delay = ($items_per_load / 4);
    $dataattribute = '';
    if (isset($data)) {
        foreach ($data as $k => $d) {
            $dataattribute .= ' data-' . $k . '="' . $d . '" ';
        }
    }
    ?>
    <div class="col-lg-12 py-5 text-center">
        <a href="#paged_<?php echo $nextpage; ?>" <?php echo $dataattribute; ?> class="btn btn-primary pager">
            <?php echo pll__('More'); ?>
            <div class="loader lds-css ng-scope">
                <div style="width:100%;height:100%" class="lds-dual-ring">
                    <div></div>
                </div>
            </div>
        </a>
    </div>
    <?php
endif;
wp_reset_query();

?>
