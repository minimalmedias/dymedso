<section class="description">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-sm-5 text-center">
                <h3 class="my-5 py-0 mx-auto"><?php echo $this->description; ?></h3>
            </div>
        </div>
    </div>
</section>