<div class="container">
    <div class="row">
        <div class="col-md-12"><h2 class="mobile-left"><?php _e('Itinéraire', 'wpml_theme'); ?></h2></div>
    </div>
    <div class="row justify-content-between">
        <div class="col-md-<?php echo($this->les_points_forts !='')?'6':'9' ?> largetext">
            <?php echo $this->itineraire; ?>
            <?php echo $this->texte_inclusions; ?>
        </div>
        <?php if($this->les_points_forts !=''): ?>
        <div class="col-md-6 col-lg-5 pointsforts">
            <h5 class="desktop verticallabel"><?php _e('Les points forts', 'wpml_theme'); ?></h5>
            <div class="m-5 icon-thumbsup"></div>
            <div class="m-5">
                <?php echo $this->les_points_forts; ?>
            </div>
        </div>
        <?php endif; ?>
    </div>
</div>