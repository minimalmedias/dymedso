<?php
require_once("../../../../wp-load.php");

$getlocale = 'fr_CA'; //get_locale() setup when WPML is configured
$query_result = array();
$taxonomy_array = array('relation' => 'AND'); //array('relation' => 'AND');
$filters = array(
    'pays'=>array(),
    'theme'=>array()
);

if (is_numeric($_POST['postid'])) {
    $template = get_page_template_slug($_POST['postid']); //or I could have just pushed page template directly in ajax data
    $terms = setCategories($template);
    $items_per_load = 2;
    $showposts = 9999;
    $taxonomy_array[] = array(
        'post_status' => 'publish',
        'taxonomy' => 'type',
        'field' => 'slug',
        'terms' => $terms,
    );
} else {
    //If homepage and postid is string
    $items_per_load = 3;
    $showposts = 3;
    $taxonomy_array[] = array(
        'post_status' => 'publish',
        'taxonomy' => 'pays',
        'field' => 'slug',
        'terms' => $_POST['postid'],
    );
}
if(isset($_POST['filters'])){
    foreach($_POST['filters'] as $taxonomy => $filter){
        $filters[$filter[0]][] = $filter[1];
    }
    foreach($filters as $tax => $query){
        if(!empty($query)){
            $taxonomy_array[] = array(
                'post_status' => 'publish',
                'taxonomy' => $tax,
                'field' => 'slug',
                'terms' => $query,
            );
        }
    }
}

$wp_query_array = array(
    'post_type' => 'voyages',
    'post_status' => 'publish',
    'showposts' => $showposts,
    'tax_query' => $taxonomy_array,
);
$wp_query = new WP_Query($wp_query_array);

// transform post data, //optimized if I were to use function format_for_template($posts, $template, $config);
if ($wp_query->have_posts()) {
    $i = 0;
    while ($wp_query->have_posts()) : $wp_query->the_post();

        $id = get_the_ID();
        $query_result[$i]['title'] = get_the_title();
        $query_result[$i]['description'] = wp_trim_words(  get_the_excerpt($id), 25, '...' );
        $query_result[$i]['url'] = get_permalink();
        $query_result[$i]['image'] = array(
            'url' => output_thumbnails($id, 'fiche'),
            'alt' => get_the_title(),
            'classes' => 'img-fluid'
        );
        $query_result[$i]['label'] = extractterm($id, 'pays');
        $i++;

    endwhile;
}

///
////////////////////////////////////////////////////////////

$firstload = $_POST['firstload'];

$chunks = array_chunk($query_result, $items_per_load);

$totalpages = count($chunks);

$nextpage = false;

if (($_POST['page'] < $totalpages) and is_numeric($_POST['postid'])) {
    $nextpage = $_POST['page'] + 1;
}

$cumulatedchunks = array();
foreach ($chunks as $index => $chunk) {
    if ($index < $_POST['page']) {
        $cumulatedchunks[] = $chunk;
    }
}

$i = 0;
foreach ($cumulatedchunks as $index => $voyages) {
    $ii = 0;
    echo '<div class="card-deck">';
    foreach ($voyages as $voyage) {
        if ($i == $_POST['page']) {
            $voyage['delay'] = 'style="animation-delay: ' . ($i / 2) . 's;"';
        }
        if ($ii == 0) {
            $voyage['rowid'] = 'paged_' . ($index + 1);
        }
        minimal_get_template_part('/templates/listing.php', $voyage);
        $ii++;
    }
    $countvoyages = intval(count($voyages));
    if ($countvoyages < $items_per_load) {
        $empties = ($items_per_load - $countvoyages);
        for ($k = 0; $k < $empties; $k++) {
            echo '<div class="card fiche offre emptycell" style="background:none"></div>';
        }
    }
    echo '</div>';
    $i++;
}

if ($nextpage != false) {
    $ajaxbutton['nextpage'] = $nextpage;
}
if (isset($items_per_load)) {
    $ajaxbutton['items_per_load'] = $items_per_load;
}
minimal_get_template_part('/templates/ajaxbutton.php', $ajaxbutton);