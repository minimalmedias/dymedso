<?php
function set_heading($return)
{
    $heading['class'] = 'col-auto';
    $heading['background'] = get_stylesheet_directory_uri() . '/assets/img/healthcare_professionals/heading.jpg';
    $heading['text'] = '<div class="text-center text-md-left m-auto pro-heading">';
    $heading['text'] .= '<h1 class="ml-md-0">'. pll__('Design to help you deliver better care').'</h1>';
    $heading['text'] .= '<img src="' . get_stylesheet_directory_uri() . '/assets/img/logo/frequencer_logo_'.pll_current_language('slug').'.png" alt="Frequencer" class="frequencerlogo ml-md-0">';
    $heading['text'] .= '</div>';
    $heading['align'] = 'justify-content-center justify-content-md-end';
    return $heading[$return];
}