<?php
function set_heading($return)
{
    $heading['class'] = 'col-lg-6';
    $heading['background'] = get_stylesheet_directory_uri() . '/assets/img/frequencer/heading.jpg';
    $heading['text'] = '<div class="text-center text-md-left m-auto frequencer-heading" id="frequenceheading">';
    $heading['text'] .= '<h1>' . pll__('The Power Of Acoustics To Provide Effective, Yet Gentle Airway Clearance Therapy') . '</h1>';
    $heading['text'] .= '<img src="' . get_stylesheet_directory_uri() . '/assets/img/about/machine.png" class="img-fluid mt-5" id="frequencermachine">';
    $heading['text'] .= '<img src="' . get_stylesheet_directory_uri() . '/assets/img/logo/frequencer_logo_' . pll_current_language('slug') . '.png" id="frequencerlogo" class="frequencerlogo" alt="Frequencer">';
    $heading['text'] .= '</div>';
    $heading['align'] = 'justify-content-start';
    return $heading[$return];
}