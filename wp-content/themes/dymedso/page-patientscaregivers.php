<?php /** Template Name: Patients & Caregivers */
global $post;
$postID = $post->ID;
get_header(); ?>
<div class="mt-lg-5">
    <?php
    minimal_get_template_part('/templates/pagenav.php', array('nav' => array(
        pll__('design-to-help') => pll__('Design to help'),
        pll__('patient-satisfaction') => pll__('Patient satisfaction'),
        pll__('testimonials') => pll__('Testimonials'),
        pll__('insurance-coverage') => pll__('Insurance coverage')
    )));
    ?>
</div>
<section class="my-md-5 mt-5 mt-md-0 pb-md-5" id="<?php echo pll__('design-to-help'); ?>">
    <?php
    echo minimal_get_template_part('/templates/image-texte.php', array(
        'image' => responsiveimage(array(
            'url' => outputimage(get_stylesheet_directory_uri() . '/assets/img/patients_and_caregivers/Image_machine_vue_1_inverse.png'),
            'alt' => pll__('Design to give you autonomy'),
            'classes' => 'img-fluid'
        )),
        'text' => array(
            'headertitle' => pll__('Design to give you autonomy'),
            'list' => array(
                pll__('Easy to operate'),
                pll__('Gentle application (Only 1.9N of force applied compare to 58N with traditional clapping)'),
                pll__('Easy to clean'),
                pll__('Four sizes of adapters to optimize treatments on patients of all ages'),
                pll__('Convenient for home use and easy to transport (includes carrying case)'),
                pll__('Certified by the FDA, UL, CE and Health Canada')
            ),
            'button2' => array(
                'label' => pll__('Request a Free Trial'),
                'url' => get_template_link('page-freetrial.php')
            )
        ),
        'layout' => 'text2-image'
    ));
    ?>
</section>
<section class="chartbg py-5" id="<?php echo pll__('patient-satisfaction'); ?>">
    <div class="container my-md-4 layout-image-text reveal revealed">
        <div class="row justify-content-center">
            <div class="col-lg-8 text-center">
                <h3 class="h2 centered mb-3 mb-md-0 mt-5"><?php echo pll__('Patient Satisfaction'); ?></h3>
                <h3><?php echo pll__('High degree of patient satisfaction with the Frequencer<sup>&reg;</sup> versus traditional CPT'); ?></h3>
                <p class="my-5"><?php echo pll__('Chest Physiotherapy Evaluation and Satisfaction Survey, compares patient’s Actual CPT method with the Frequencer. For both therapies, patient answers questions by choosing a number from 1 to 5 (Strongly Disagree = 1, Strongly Agree = 5). Dymedso gathered between 50 and 60 patient surveys and results show patients prefer the Frequencer for many reasons.'); ?>
            </div>
        </div>
        <?php minimal_get_template_part('/templates/graphs.php'); ?>
        <div class="row justify-content-center">
            <div class="col-md-8 text-center">
                <h3 class="mt-5"><?php echo pll__('Tell us how much you love your Frequencer'); ?></h3>
            </div>
        </div>
        <div class="row justify-content-center  my-4">
            <div class="col-md-4 text-center">
                <a href="<?php
                $link1['fr'] = 'index.php?pagename=contactez-nous&formid=demande-generale&selectform=patient_and_caregiver';
                $link1['en'] = '?pagename=contact&formid=general-inquiry&selectform=patient-and-caregiver';
                echo $link1[ pll_current_language('slug')];
                ?>#contactform" class="btn btn-primary w-100 mx-auto">
               <?php echo pll__("I'm a patient"); ?>&nbsp;&nbsp;&nbsp;&nbsp;
                </a>
            </div>
            <div class="col-md-4 text-center">
                <a href="<?php
                $link2['fr'] = 'index.php?pagename=contactez-nous&formid=demande-generale&selectform=healthcare_professional';
                $link2['en'] = '?pagename=contact&formid=general-inquiry&selectform=healthcare_professional';
                echo $link2[ pll_current_language('slug')];
                ?>#contactform" class="btn btn-primary w-100 mx-auto">
                 <?php echo pll__("I'm a professional"); ?>&nbsp;&nbsp;&nbsp;&nbsp;
                </a>
            </div>
        </div>
    </div>
</section>
<?php minimal_get_template_part('/templates/testimonials.php', array('classes' => '')); ?>
<section class="pt-5" id="<?php echo pll__('insurance-coverage'); ?>">
    <?php
    echo responsiveimage(array(
        'url' => outputimage(get_stylesheet_directory_uri() . '/assets/img/patients_and_caregivers/Image_machine_vue_2.png'),
        'alt' => pll__('Design to give you autonomy'),
        'classes' => 'img-fluid overedge'
    ))
    ?>
    <div class="container mt-5 my-md-0">
        <div class="row justify-content-center align-items-center">
            <div class="col-lg-5 text-center">
                <h2 class="mt-5 mt-lg-0 mb-md-4"><?php echo pll__('Insurance Coverage'); ?></h2>
                <h3><?php echo pll__('If you have insurance coverage, contact us and we will send you forms.'); ?></h3>
                <p class="p-5">
                    <a href="<?php echo get_template_link('page-contact.php'); ?>"
                       class="btn btn-primary"><?php echo pll__('Contact Us'); ?></a>
                </p>
            </div>
        </div>
    </div>
</section>
<?php get_footer(); ?>
