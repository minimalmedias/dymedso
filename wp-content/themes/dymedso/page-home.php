<?php /** Template Name: Home */
global $post;
$postID =  $post->ID;
get_header();
?>
<section class="heading">
    <?php minimal_get_template_part('/components/carousel.php', array(
        'id' => 'heading',
        'interval' => 10000,
        'template' => '/templates/homeslides.php',
        'indicators' => 1,
        'items' => array(array('id'=>1),array('id'=>2),array('id'=>3))
    )); ?>
</section>
<section>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-10 text-center blurb p-5 mb-lg-5">
                <p><?php echo pll__('Dymedso is committed to the introduction of new medical technologies for airway clearance'); ?></p>
            </div>
        </div>
    </div>
</section>
<?php
$c = 0;
foreach (array(
             array(
                 'subtitle' => pll__('Breathe more freely'),
                 'title' => pll__('Indicated for a broad range of patients'),
                 'text' => pll__('The Frequencer<sup>&reg;</sup> is recommended for patients having respiratory ailments which involve defective mucociliary clearance as is typical in patients suffering from cystic fibrosis as well as chronic bronchitis, bronchiectasis, ciliary dyskinesia syndromes, asthma, muscular dystrophy, neuromuscular degenerative disorders, post-operative atelectasis and thoracic wall defects.'),
                 'button_label' => pll__('Patient or Caregivers'),
                 'button_link' => get_template_link('page-patientscaregivers.php'),
                 'image' => get_stylesheet_directory_uri() . '/assets/img/home/image_1.jpg'
             ),
             array(
                 'subtitle' => pll__('ENHANCE THE HEALTH AND WELL BEING OF YOUR PATIENTS'),
                 'title' => pll__('A digitally controlled acoustic airway clearance device'),
                 'text' => pll__('The Frequencer® provides airway clearance therapy and promotes bronchial drainage by inducing vibration in the chest walls. The Frequencer® provides a gentler, less painful form of therapy from the traditional “clapping” method of postural drainage therapy, allowing it to be used on patients who cannot be treated by clapping.'),
                 'button_label' => pll__('Healthcare Professionals'),
                 'button_link' => get_template_link('page-healthcareprofessionals.php'),
                 'image' => get_stylesheet_directory_uri() . '/assets/img/home/image_2.jpg'
             ),
             array(
                 'subtitle' => pll__('INVESTING IN BETTER OUTCOMES'),
                 'title' => pll__('The Frequencer<sup>&reg;</sup> shows benefits for institutions'),
                 'text' => pll__('For hospitals already acquainted with the benefits of ultrasound and scanner which operates at very high frequency the Frequencer® using very low frequency offers a completely new range of airway clearance possibilities.'),
                 'button_label' => pll__('Healthcare Institutions'),
                 'button_link' => get_template_link('page-healthcareinstitutions.php'),
                 'image' => get_stylesheet_directory_uri() . '/assets/img/home/image_3.jpg'
             )
         ) as $im):
    echo '<section class="mb-5 pb-5">';
    minimal_get_template_part('/templates/image-texte.php', array(
        'image' => responsiveimage(array(
            'url' => outputimage($im['image']),
            'alt' => $im['title'],
            'classes' => 'img-fluid'
        )),
        'text' => array(
            'subtitle' => $im['subtitle'],
            'title' => $im['title'],
            'text' => $im['text'],
            'button' => array(
                'label' => $im['button_label'],
                'url' => $im['button_link']
            )
        ),
        'layout' => (++$c % 2) ? 'text-image' : 'image-text'
    ));
    echo '</section>';
endforeach;
?>
<section class="bg-paleblue">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-8 text-center p-5 mb-md-5">
                <h2 class="m-md-5"><?php echo pll__('Our technology'); ?></h2>
                <h3><?php echo pll__('Using the power of acoustics to provide effective, yet gentle airway clearance therapy.'); ?></h3>
                <?php echo responsiveimage(array(
                    'url' => outputimage(get_stylesheet_directory_uri() . '/assets/img/home/Image_machine_vue_2.png'),
                    'alt' => pll__('Our technology'), 'classes' => 'img-fluid m-xl-5'
                )); ?>
                <p><a href="<?php echo get_template_link('page-freetrial.php'); ?>" class="btn btn-primary">
                        <?php echo pll__('Request a Free Trial'); ?>
                    </a></p>
            </div>
        </div>
    </div>
</section>


<?php /*
<header class="masthead text-center text-white d-flex align-items-center">
    <div class="container">
        <div class="row">
            <div class="col-lg-10 mx-auto">
                <h1 class="text-uppercase">
                    <strong>Minimal Bootstrap Starter Theme</strong>
                </h1>
                <hr>
            </div>
            <div class="col-lg-8 mx-auto">
                <p class="text-faded mb-5">Start Bootstrap can help you build better websites using the Bootstrap CSS framework! Just download your template and start going, no strings attached!</p>
                <a class="btn btn-primary btn-xl js-scroll-trigger" href="#about">Find Out More</a>
            </div>
        </div>
    </div>
</header>

<section class="bg-primary emerge-top emerge-speed-1" id="about" data-emergence="hidden">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 mx-auto text-center">
                <h2 class="section-heading text-white">We've got what you need!</h2>
                <hr class="light my-4">
                <p class="text-faded mb-4">Start Bootstrap has everything you need to get your new website up and running in no time! All of the templates and themes on Start Bootstrap are open source, free to download, and easy to use. No strings attached!</p>
                <a class="btn btn-light btn-xl js-scroll-trigger" href="#services">Get Started!</a>
            </div>
        </div>
    </div>
</section>

<section id="services" class="emerge-top emerge-speed-1" data-emergence="hidden">
    <div class="container">
        <div class="row" >
            <div class="col-lg-12 text-center">
                <h2 class="section-heading">At Your Service</h2>
                <hr class="my-4">
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-6 text-center">
                <div class="service-box mt-5 mx-auto">
                    <i class="fa fa-4x fa-diamond text-primary mb-3 sr-icons"></i>
                    <h3 class="mb-3">Sturdy Templates</h3>
                    <p class="text-muted mb-0">Our templates are updated regularly so they don't break.</p>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 text-center">
                <div class="service-box mt-5 mx-auto">
                    <i class="fa fa-4x fa-paper-plane text-primary mb-3 sr-icons"></i>
                    <h3 class="mb-3">Ready to Ship</h3>
                    <p class="text-muted mb-0">You can use this theme as is, or you can make changes!</p>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 text-center">
                <div class="service-box mt-5 mx-auto">
                    <i class="fa fa-4x fa-newspaper-o text-primary mb-3 sr-icons"></i>
                    <h3 class="mb-3">Up to Date</h3>
                    <p class="text-muted mb-0">We update dependencies to keep things fresh.</p>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 text-center">
                <div class="service-box mt-5 mx-auto">
                    <i class="fa fa-4x fa-heart text-primary mb-3 sr-icons"></i>
                    <h3 class="mb-3">Made with Love</h3>
                    <p class="text-muted mb-0">You have to make your websites with love these days!</p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="p-0  emerge-top emerge-speed-1" id="portfolio" data-emergence="hidden">
    <div class="container-fluid p-0">
        <div class="row no-gutters popup-gallery">
            <div class="col-lg-4 col-sm-6">
                <a class="portfolio-box" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/portfolio/fullsize/1.jpg">
                    <img class="img-fluid" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/portfolio/thumbnails/1.jpg" alt="">
                    <div class="portfolio-box-caption">
                        <div class="portfolio-box-caption-content">
                            <div class="project-category text-faded">
                                Category
                            </div>
                            <div class="project-name">
                                Project Name
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-sm-6">
                <a class="portfolio-box" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/portfolio/fullsize/2.jpg">
                    <img class="img-fluid" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/portfolio/thumbnails/2.jpg" alt="">
                    <div class="portfolio-box-caption">
                        <div class="portfolio-box-caption-content">
                            <div class="project-category text-faded">
                                Category
                            </div>
                            <div class="project-name">
                                Project Name
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-sm-6">
                <a class="portfolio-box" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/portfolio/fullsize/3.jpg">
                    <img class="img-fluid" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/portfolio/thumbnails/3.jpg" alt="">
                    <div class="portfolio-box-caption">
                        <div class="portfolio-box-caption-content">
                            <div class="project-category text-faded">
                                Category
                            </div>
                            <div class="project-name">
                                Project Name
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-sm-6">
                <a class="portfolio-box" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/portfolio/fullsize/4.jpg">
                    <img class="img-fluid" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/portfolio/thumbnails/4.jpg" alt="">
                    <div class="portfolio-box-caption">
                        <div class="portfolio-box-caption-content">
                            <div class="project-category text-faded">
                                Category
                            </div>
                            <div class="project-name">
                                Project Name
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-sm-6">
                <a class="portfolio-box" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/portfolio/fullsize/5.jpg">
                    <img class="img-fluid" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/portfolio/thumbnails/5.jpg" alt="">
                    <div class="portfolio-box-caption">
                        <div class="portfolio-box-caption-content">
                            <div class="project-category text-faded">
                                Category
                            </div>
                            <div class="project-name">
                                Project Name
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-sm-6">
                <a class="portfolio-box" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/portfolio/fullsize/6.jpg">
                    <img class="img-fluid" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/portfolio/thumbnails/6.jpg" alt="">
                    <div class="portfolio-box-caption">
                        <div class="portfolio-box-caption-content">
                            <div class="project-category text-faded">
                                Category
                            </div>
                            <div class="project-name">
                                Project Name
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
</section>

<section class="bg-dark text-white emerge-top emerge-speed-1" data-emergence="hidden">
    <div class="container text-center">
        <h2 class="mb-4">Free Download at Start Bootstrap!</h2>
        <a class="btn btn-light btn-xl sr-button" href="http://startbootstrap.com/template-overviews/creative/">Download Now!</a>
    </div>
</section>

<section id="contact" class="emerge-bottom emerge-speed-1" data-emergence="hidden">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 mx-auto text-center">
                <h2 class="section-heading">Let's Get In Touch!</h2>
                <hr class="my-4">
                <p class="mb-5">Ready to start your next project with us? That's great! Give us a call or send us an email and we will get back to you as soon as possible!</p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 ml-auto text-center">
                <i class="fa fa-phone fa-3x mb-3 sr-contact"></i>
                <p>123-456-6789</p>
            </div>
            <div class="col-lg-4 mr-auto text-center">
                <i class="fa fa-envelope-o fa-3x mb-3 sr-contact"></i>
                <p>
                    <a href="mailto:your-email@your-domain.com">feedback@startbootstrap.com</a>
                </p>
            </div>
        </div>
    </div>
</section>
 */ ?>

<?php minimal_get_template_part('/templates/testimonials.php', array('classes' => 'spaced-top')); ?>

<?php get_footer(); ?>
