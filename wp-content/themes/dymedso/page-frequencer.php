<?php /** Template Name: Frequencer */
global $post;
$postID =  $post->ID;
$manual1 = get_field('field_5a69f624624ed1',trueid($postID));
$manual2 = get_field('field_5a69f624624ed2',trueid($postID));
$manual3 = get_field('field_5a69f624624ed3',trueid($postID));
$manual4 = get_field('field_5a69f624624ed4',trueid($postID));
$j = 0;
$instructions1 = array(
    array('title' => pll__('Indications'), 'list' => array()),
    array('title' => pll__('Contraindications'), 'list' => array())
);
if (have_rows('field_5a68fbd334e64',trueid($postID))) {
    while (have_rows('field_5a68fbd334e64',trueid($postID))) {
        the_row();
        if (get_sub_field('field_5a68fbe534e65') != '') {
            $instructions1[0]['list'][] = get_sub_field('field_5a68fbe534e65');
        }
    }
}
if (have_rows('field_5a68fbf99150e',trueid($postID))) {
    while (have_rows('field_5a68fbf99150e',trueid($postID))) {
        the_row();
        if (get_sub_field('field_5a68fc139150f') != '') {
            $instructions1[1]['list'][] = get_sub_field('field_5a68fc139150f');
        }
    }
}

$i = 0;
$instructions2 = array(
    array('title' => pll__('Indications'), 'list' => array()),
    array('title' => pll__('Contraindications'), 'list' => array())
);
if (have_rows('field_5a68fbd334e64b',trueid($postID))) {
    while (have_rows('field_5a68fbd334e64b',trueid($postID))) {
        the_row();
        if (get_sub_field('field_5a68fbe534e65b') != '') {
            $instructions2[0]['list'][] = get_sub_field('field_5a68fbe534e65b');
        }
    }
}
if (have_rows('field_5a68fbf99150eb',trueid($postID))) {
    while (have_rows('field_5a68fbf99150eb',trueid($postID))) {
        the_row();
        if (get_sub_field('field_5a68fc139150fb') != '') {
            $instructions2[1]['list'][] = get_sub_field('field_5a68fc139150fb');
        }
    }
}
get_header();
?>
<script>
    function showNorthAmerica() {
        document.getElementById("northamerica").classList.remove("d-none");
        document.getElementById('choisirregion').classList.add('d-none');
        document.getElementById("international").classList.add("d-none");
        document.getElementById("btnInternational").classList.remove("active");
        document.getElementById("btnNorthAmerica").classList.add("active");
        document.getElementById("navigation-region").classList.remove("d-none");
    }

    function showInternational() {
        document.getElementById("international").classList.remove("d-none");
        document.getElementById('choisirregion').classList.add('d-none');
        document.getElementById("northamerica").classList.add("d-none");
        document.getElementById("btnInternational").classList.add("active");
        document.getElementById("btnNorthAmerica").classList.remove("active");
        document.getElementById("navigation-region").classList.remove("d-none");
    }
</script>
<div class="mt-lg-5">
    <?php
    minimal_get_template_part('/templates/pagenav.php', array('nav' => array(
        pll__('what-is-the-frequencer') => pll__('What is the Frequencer'),
        pll__('how-does-it-work') => pll__('How does it work?'),
        pll__('indications-contraindications') => pll__('Indications & Contraindications'),
        pll__('research-findings') => pll__('Research findings'),
        pll__('user-manual') => pll__('User manual')
    )));
    ?>

</div>
<section id="<?php echo pll__('what-is-the-frequencer'); ?>">
    <div class="container">
        <div class="row justify-content-center align-items-center">
            <div class="col-md-8 text-center">
                <h2 class="mt-md-5 mt-lg-0 mb-4"><?php the_title(); ?></h2>
                <p class="my-md-5"><?php the_content(); ?></p>
                <p class="p-lg-5 my-5">
                    <a href="<?php echo get_template_link('page-contact.php'); ?>" class="btn btn-primary">
                        <?php echo pll__('Information for Patients'); ?>
                    </a>
                </p>
            </div>
        </div>
    </div>
</section>
<section class="bg-paleblue pt-5" id="<?php echo pll__('how-does-it-work'); ?>">
    <div class="container mt-lg-5">
        <div class="row justify-content-center align-items-center">
            <div class="col-md-8 text-center">
                <h2 class="mt-lg-5 mt-lg-0 mb-md-4"><?php echo pll__('How does the Frequencer®  work?'); ?></h2>
                <p class="my-md-5"><?php echo pll__('For hospitals, already acquainted with ultrasound and scanners operating at very high frequencies, The Frequencer® generates low frequency within the range of 20-65Hz and offers an adjustable intensity based on the patient condition. Sound used at low and very low frequency offers a completely new range of specific and targeted treatment possibilities in airway clearance.'); ?></p>
            </div>
        </div>
        <div class="row justify-content-center mt-5">
            <?php $i = 1;
            foreach (array(
                         array(
                             'img' => responsiveimage(array(
                                 'url' => outputimage(get_stylesheet_directory_uri() . '/assets/img/frequencer/image_1.jpg'),
                                 'alt' => pll__('Quick and easy set-up to begin treatment'),
                                 'classes' => 'card-img-top'
                             )),
                             'title' => pll__('Quick and easy set-up to begin treatment'),
                             'text' => pll__('It only takes a few seconds for the patient or therapist to select individual settings and start therapy.')
                         ),
                         array(
                             'img' => responsiveimage(array(
                                 'url' => outputimage(get_stylesheet_directory_uri() . '/assets/img/frequencer/image_2.jpg'),
                                 'alt' => pll__('Low frequency sound penetrates deep into the chest.'),
                                 'classes' => 'card-img-top'
                             )),
                             'title' => pll__('Low frequency sound penetrates deep into the chest.'),
                             'text' => pll__('Low frequency greatly reduce mucus viscosity and promote mucus flow.')
                         )
                     ) as $steps): ?>
                <div class="col-md-4 card noclick">
                    <?php echo $steps['img']; ?>
                    <div class="card-body text-center">
                        <h6><?php echo pll__('Step') . ' ' . $i; ?></h6>
                        <h5 class="card-title text-center fiche"><?php echo $steps['title']; ?></h5>
                        <p class="card-text"><?php echo $steps['text']; ?></p>
                    </div>
                </div>
                <?php $i++; endforeach; ?>
        </div>
        <div class="row justify-content-center align-items-center">
            <div class="col-md-8 text-center">
                <p class="my-md-5"><?php echo pll__('In hospital settings, four sizes of single-patient adapters are available. For home usage the Frequencer® comes with four sizes of permanent adapters. These adapters optimize treatment on patients of all ages and conditions.'); ?></p>
                <p class="p-lg-5 mt-5">
                    <a href="<?php echo get_template_link('page-freetrial.php'); ?>"
                       class="btn btn-primary"><?php echo pll__('Request a Free Trail'); ?></a>
                </p>
            </div>
        </div>
    </div>
</section>
<section class="bg-paleblue pt-lg-5" id="<?php echo pll__('indications-contraindications'); ?>">
<div class="container mt-lg-5">
    <div class="row justify-content-center align-items-center">
        <div class="col-md-8 text-center">
            <h2 class="mt-5 mt-lg-0 mb-md-4"><?php echo pll__('Indications & Contraindications'); ?></h2>
            <p class="my-md-5"><?php echo pll__('Many patient population due to their unique or vulnerable condition may not always be appropriate for conventional airway clearance treatments. The Frequencer® provides the RT with the ability to treat the underline airway clearance condition and providing a solution for these patients.'); ?></p>
        </div>
    </div>
    <div class="row justify-content-center">
        <div id="choisirregion" class="col-md-5 w-100 card noclick">
            <div class="card-body text-center">
                <h3 class="mb-5"><?php echo pll__('Choose your region'); ?></h3>
                <div class="col-md-12 text-center">
                    <p class="p-lg-1">
                        <input type="button" onclick="showNorthAmerica()" name="northAmerica" class="btn btn-primary w-100" value="<?php echo pll__('North America'); ?>">
                    </p>
                </div>
                <div class="col-md-12 text-center">
                    <p class="p-lg-1">
                    <input type="button" onclick="showInternational()" name="international" class="btn btn-primary w-100" value="<?php echo pll__('International'); ?>">
                    </p>
                </div>
            </div><!-- fin de card-body -->
        </div><!-- fin de card -->
    </div>
    <div id="navigation-region" class="w-100 d-none text-center mb-5">
        <div class="nav-region">
            <input type="button" onclick="showNorthAmerica()" id="btnNorthAmerica" name="northAmerica" class="nav-region-link" value="<?php echo pll__('North America'); ?>">
            <input type="button" onclick="showInternational()" id="btnInternational" name="international" class="nav-region-link" value="<?php echo pll__('International'); ?>">
        </div>
    </div>
    <div class="row justify-content-center">
        <div id="northamerica" class="row col-md-12 justify-content-center d-none">
            <?php foreach ($instructions1 as $instruction1): ?>
                <div class="col-md-5 card noclick">
                    <div class="card-body">
                        <h5 class="card-title text-center"><?php echo $instruction1['title']; ?></h5>
                        <ul>
                            <?php foreach ($instruction1['list'] as $li): ?>
                                <li><span><?php echo $li; ?></span></li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                </div>
            <?php $j++; endforeach;  ?>
        </div>
        <div id="international" class="row col-md-12 justify-content-center d-none">
            <?php foreach ($instructions2 as $instruction2): ?>
                <div class="col-md-5 card noclick">
                    <div class="card-body">
                        <h5 class="card-title text-center"><?php echo $instruction2['title']; ?></h5>
                        <ul>
                            <?php foreach ($instruction2['list'] as $li): ?>
                                <li><span><?php echo $li; ?></span></li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                </div>
            <?php $j++; endforeach;  ?>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-8 text-center">
                <p class="p-lg-5 my-5">
                    <a href="<?php echo get_template_link('page-freetrial.php'); ?>"
                        class="btn btn-primary"><?php echo pll__('Request a Free Trial'); ?></a>
                </p>
        </div>
    </div>
</div>
</section>
<section class="bg-gradientblue" id="<?php echo pll__('research-findings'); ?>">
    <div class="container text-center p-3">
        <div class="row justify-content-lg-center">
            <div class="col-lg-8">
                <h2 class="m-5 text-white"><?php echo pll__('Research findings'); ?></h2>
                <?php

                $items = array();
                $wp_query = new WP_Query(array('post_status' => 'publish', 'post_type' => 'reports','posts_per_page'=>'-1'));
                if ($wp_query->have_posts()):
                    while ($wp_query->have_posts()) : $wp_query->the_post();
                        $id = get_the_ID();
                        $label = frequencer(get_field('field_5a68dfc23c946',trueid($id)));
                        $file = get_field('field_5a68dfd03c947',$id);
                        $items[$id]['author'] = '<a class="btn btn-reverse" target="_blank" href="' . $file . '">' . $label . '</a>';
                        $items[$id]['text'] = frequencer(get_field('field_5a68dfb13c945',trueid($id)));
                    endwhile;
                endif;
                wp_reset_postdata();
                minimal_get_template_part('/components/carousel.php', array(
                    'id' => 'studies', 'interval' => 10000, 'template' => '/templates/testimonial.php', 'indicators' => 1, 'nav' => 1,
                    'items' => $items));

                ?>
                <p class="pt-5">&nbsp;</p>
            </div>
        </div>
    </div>
</section>
<section class="pt-lg-5 pb-5" id="<?php echo pll__('user-manual'); ?>">
    <div class="container mb-4">
        <div class="row justify-content-center align-items-center">
            <div class="col-md-8 text-center">
                <h2 class="mt-5 mt-lg-0 mb-md-4"><?php echo pll__('User Manual'); ?></h2>
                <h4 class="mb-5"><?php echo pll__('Download the Frequencer Manual'); ?></h4>
                <?php if($manual1 != '') { echo'<p>
                    <a class="btn-link regionset" target="_blank"
                       href="'. $manual1 . '">'. pll__('English (United States)') . '</a>
                </p>'; } ?>
                <?php if($manual2 != '') { echo'<p>
                    <a class="btn-link regionset" target="_blank"
                       href="'. $manual2 . '">'. pll__('English (International)') . '</a>
                </p>'; } ?>
                <?php if($manual3 != '') { echo'<p>
                    <a class="btn-link regionset" target="_blank"
                       href="'. $manual3 . '">'. pll__('French') . '</a>
                </p>'; } ?>
               <?php if($manual4 != '') { echo'<p>
                    <a class="btn-link regionset" target="_blank"
                       href="'. $manual4 . '">'. pll__('Deutch') . '</a>
                </p>'; } ?>
            </div>
        </div>
    </div>
</section>
<style>
    .carousel-inner .carousel-item p:first-child{
        font-weight: bold;
    }
    .btn-reverse {
        font-size: 16px;
    }

</style>
<?php get_footer(); ?>

