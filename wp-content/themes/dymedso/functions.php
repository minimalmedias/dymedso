<?php
$parent =  $_SERVER['DOCUMENT_ROOT'].'/wp-content/themes/minimal-bootstrap';
require 'functions/functions-menus.php';
require 'functions/functions-setup.php';
require 'functions/functions-taxonomies.php';
require 'functions/functions-acf.php';
require 'functions/functions-custom-post-types.php';
require 'functions/functions-general.php';
require 'functions/functions-navbar.php';
require 'functions/functions-sitemap.php';
require 'functions/functions-region.php';
//require 'functions/functions-setup-menus-pages.php'; // automaticaly create starter menu and pages. Work in progress
//require $parent.'/functions/gallery-metabox.php'; //Option for adding gallery in admin