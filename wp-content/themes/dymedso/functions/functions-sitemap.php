<?php

class dymedso_walker_sitemap extends Walker_Nav_menu
{

    function start_lvl(&$output, $depth = 0, $args = array())
    { // ul
        $indent = str_repeat("\t", $depth); // indents the outputted HTML
        $submenu = ($depth > 0) ? ' sub-menu' : 'tree';
        $output .= "\n$indent<ul class=\"tree sublist $submenu depth_$depth\">\n";
    }

    function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0)
    { // li a span

        $indent = ($depth) ? str_repeat("\t", $depth) : '';
        $attributes = '';
        $li_attributes = '';
        $class_names = $value = '';

        $navitem_li_classes = (isset($args->navitem_li_classes))?esc_attr( $args->navitem_li_classes ):'';
        $navitem_a_classes = (isset($args->navitem_a_classes))?esc_attr( $args->navitem_a_classes ):'nav-link';

        $classes = empty($item->classes) ? array() : (array)$item->classes;
        $classes[] = ($args->walker->has_children) ? 'sublist' : '';
        $classes[] = ($item->current || $item->current_item_anchestor) ? 'active' : '';
        $classes[] = 'nav-item';
        $classes[] = 'nav-item-' . $item->ID;
        if ($depth && $args->walker->has_children) {
            $classes[] = 'sublist';
        }

        $class_names = join(' ', apply_filters('nav_menu_css_class', array_filter($classes), $item, $args));
        $class_names = ' class="' . esc_attr($class_names) . $navitem_li_classes . '"';

        $id = apply_filters('nav_menu_item_id', 'menu-item-' . $item->ID, $item, $args);
        $id = strlen($id) ? ' id="' . esc_attr($id) . '"' : '';

        $output .= $indent . '<li ' . $id . $value . $class_names . $li_attributes . '>';

        global $post;
        $url = esc_attr($item->url);
        if($url[0] == '#') {
            $parent_item_page_id = get_post_meta($item->menu_item_parent, '_menu_item_object_id', true);
            $parentlink = get_permalink($parent_item_page_id);
            if ($parent_item_page_id != $post->ID) {
                $url = $parentlink.$url;
            }
        }

        $attributes .= !empty($item->attr_title) ? ' title="' . esc_attr($item->attr_title) . '"' : '';
        $attributes .= !empty($item->target) ? ' target="' . esc_attr($item->target) . '"' : '';
        $attributes .= !empty($item->xfn) ? ' rel="' . esc_attr($item->xfn) . '"' : '';
        $attributes .= !empty($item->url) ? ' href="' . $url . '"' : '';
        $attributes .= ' class="'.$navitem_a_classes.'"';

        $replacethis = array('Frequencer®');
        $replacedwith = array('Frequencer<sup>&reg;</sup>');

        $item_output = $args->before;
        $item_output .= ($depth > 0) ? '<a class="sub-item"' . $attributes . '>' : '<a' . $attributes . '>';
        $item_output .= $args->link_before . str_replace($replacethis,$replacedwith,apply_filters('the_title', $item->title, $item->ID)) . $args->link_after;
        $item_output .= '</a>';
        $item_output .= $args->after;

        $output .= apply_filters('walker_nav_menu_start_el', $item_output, $item, $depth, $args);

    }

}