<?php
/*  sample taxonomy and custom post type

add_action('init', 'create_theme_hierarchical_taxonomy', 0);
add_action('init', 'my_custom_post_blog');

function create_theme_hierarchical_taxonomy()
{

    $labels = array(

        'name' => _x('Thèmes', 'taxonomy general name'),
        'singular_name' => _x('Thème', 'taxonomy singular name'),
        'search_items' => __('Chercher parmis les thèmes'),
        'all_items' => __('Tous les thèmes'),
        'parent_item' => __('Thème parent'),
        'parent_item_colon' => __('Thèmes parent:'),
        'edit_item' => __('Modifier thème'),
        'update_item' => __('Mettre à jour'),
        'add_new_item' => __('Ajouter'),
        'new_item_name' => __('Nouveau thème'),
        'menu_name' => __('Thèmes'),

    );

    register_taxonomy('theme', array(), array(

        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'theme'),

    ));
}
function my_custom_post_blog()
{
    $labels = array(
        'name' => _x('Voyages', 'post type general name', 'source'),
        'singular_name' => _x('Voyage', 'post type singular name', 'source'),
        'add_new' => _x('Ajouter', 'book'),
        'add_new_item' => __('Ajouter un voyage'),
        'edit_item' => __('Modifier cette fiche de voyage'),
        'new_item' => __('Nouveau'),
        'all_items' => __('Tous les voyages'),
        'view_item' => __('Afficher la fiche de voyage'),
        'search_items' => __('Chercher parmis les voyages'),
        'not_found' => __('Aucun voyage trouvée'),
        'not_found_in_trash' => __('Aucun voyage trouvée dans la corbeille'),
        'parent_item_colon' => '',
        'menu_name' => 'Voyages'
    );
    $args = array(
        'labels' => $labels,
        'description' => 'Voyages',
        'public' => true,
        'menu_position' => 5,
        'supports' => array('title', 'editor', 'thumbnail', 'excerpt', 'comments'),
        'taxonomies' => array('pays', 'theme', 'type', 'compagnie'),
        'has_archive' => true,
        'menu_icon' => 'dashicons-palmtree',
    );
    register_post_type('voyages', $args);
}

*/