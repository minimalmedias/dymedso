<?php
add_action('wp_enqueue_scripts', 'client_enqueues', 200);
add_action('pre_get_search_form', 'search_form_no_filters');
add_filter('excerpt_more', 'new_excerpt_more');
add_filter('image_resize_dimensions', 'image_crop_dimensions', 10, 6); // allow scaling up for uploaded images
//add_action('wp_head', 'bootstrap4_ie8fix');
add_filter('wpseo_metabox_prio', 'yoasttobottom');

set_post_thumbnail_size(600, 400, true); // default Featured Image dimensions (cropped)
add_image_size('header', 1200, 600, true);
add_image_size('newsthumbnail', 600, 400, true);

function client_enqueues()
{
    wp_dequeue_script('b4st-js');
    wp_dequeue_script('modernizr'); //decomment for prod
    gravity_form_enqueue_scripts(4, true); //important if using Gravity Forms to avoid JS error
    // All site CSS combined and minified into style.css via Sass & Gulp
    wp_register_style('client', get_stylesheet_directory_uri() . '/assets/css/style.css', false, null);
    wp_enqueue_style('client');
    wp_register_script('client-js', get_stylesheet_directory_uri() . '/assets/js/scripts.js', false, null, true);
    wp_enqueue_script('client-js');
}

function hide_editor()
{
    $templates = array();
    if (!empty($templates)) {
        $post_id = '';
        isset($_GET['post']) ? $post_id = $_GET['post'] : (isset($_POST['post_ID']) ? $post_id = $_POST['post_ID'] : '');
        if ($post_id == '') return;
        $template_file = basename(get_page_template());
        foreach ($templates as $template) {
            if ($template_file == 'page-' . $template . '.php') {
                remove_post_type_support('page', 'editor');
            }
        }
    }
}

function search_form_no_filters()
{
    // look for local searchform template
    $search_form_template = locate_template('searchform.php');
    if ('' !== $search_form_template) {
        // searchform.php exists, remove all filters
        remove_all_filters('get_search_form');
    }
}

function bootstrap4_ie8fix()
{
    echo '
    <!--[if lt IE 10]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link href="' . get_stylesheet_directory_uri() . '/assets/css/bootstrap-ie8.css" rel="stylesheet">
    <![endif]-->
    ';
}

function yoasttobottom()
{
    return 'low';
}

function new_excerpt_more($more)
{
    return '...';
}

