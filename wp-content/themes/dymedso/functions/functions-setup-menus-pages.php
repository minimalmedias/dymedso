<?php
// setup starter pages and menus

$menus = array(
    'mainmenu' => 'mainmenu',
    'footermenu' => 'footermenu',
);
$pages = array(
    array('Home', 'page-home.php', 'show_on_front'), //page template must be used for home
    array('About Us', 'page-about.php'),
    array('Contact Us')
);

unregister_nav_menu('Navbar'); //remove minimal-bootstrap menu
register_nav_menus($menus);

if (isset($_GET['activated']) && is_admin()) {
    create_starter_pages($pages);
    //create_menus(); // TODO: finish this as not fully functional, locations need to be set manually
}

function create_starter_pages($pages)
{
    $i = 0;
    foreach ($pages as $page) {
        if (!wp_exist_post_by_title($page[0])) {
            $pagedetails = array(
                'post_type' => 'page',
                'post_title' => $page[0],
                'post_content' => '',
                'post_status' => 'publish',
                'post_author' => 1
            );
            $page_id = wp_insert_post($pagedetails);
            if (isset($page[1])) {
                $template = $page[1];
            } else {
                $template = 'page.php';
            }
            update_post_meta($page_id, '_wp_page_template', $template);
            if (isset($page[2]) and $page[2] == 'show_on_front') {
                update_option('page_on_front', $page_id);
                update_option('show_on_front', 'page');
            }
            $i++;
        }
    }
}

function the_slug_exists($post_name) {
    global $wpdb;
    if($wpdb->get_row("SELECT post_name FROM wp_posts WHERE post_name = '" . $post_name . "'", 'ARRAY_A')) {
        return true;
    } else {
        return false;
    }
}

function wp_exist_post_by_title($title)
{
    global $wpdb;
    $return = $wpdb->get_row("SELECT ID FROM wp_posts WHERE post_title = '" . $title . "' && post_status = 'publish' && post_type = 'post' ", 'ARRAY_N');
    if (empty($return)) {
        return false;
    } else {
        return true;
    }
}

function create_menus()
{
    $menus = get_registered_nav_menus();
    $pages = get_pages();
    foreach ($menus as $name) {
        $menu_exists = wp_get_nav_menu_object($name);
        if (!$menu_exists) {
            $menu_id = wp_create_nav_menu($name);
            if (count($pages) > 0) {
                $i=0;
                foreach ($pages as $page) {
                    wp_update_nav_menu_item($menu_id, 0, array(
                            'menu-item-title' => $page->post_title.' TEST',
                            'menu-item-object-id' => $page->post_id,
                            'menu-item-db-id' => 0,
                            'menu-item-object' => 'page',
                            'menu-item-parent-id' => 0,
                            'menu-item-depth' => 1,
                            'menu-item-type' => 'page',
                            'menu-item-url' => 'test', //get_page_link($page->post_id),
                            'menu-item-status' => 'publish')
                    );
                }
            }
        }
        // TODO: fix this, not working
        $locations = get_theme_mod('nav_menu_locations');
        foreach($locations as $locationId => $menuValue){
            $locations[$locationId] = $name;
        }
        set_theme_mod('nav_menu_locations', $locations);
        ////////
    }
}