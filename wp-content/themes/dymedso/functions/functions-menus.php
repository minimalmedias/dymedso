<?php
function register_the_menus()
{
    register_nav_menus(array(
        'submenu' => 'submenu',
        'mainmenu' => 'mainmenu',
        'footer1' => 'footer1',
        'footer2' => 'footer2',
        'footer3' => 'footer3',
        'sitemap' => 'sitemap'
    ));
}
add_action('after_setup_theme', 'register_the_menus');