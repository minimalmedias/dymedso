<?php

add_theme_support('title-tag');
add_theme_support('post-thumbnails');
add_filter( 'body_class', function( $classes ) {return array_merge( $classes, array(
    'starter' //add classes to body here
) );} );

add_filter( 'the_content', 'filter_the_content_in_the_main_loop' );
add_filter( 'the_title', 'filter_the_content_in_the_main_loop' );

function filter_the_content_in_the_main_loop( $content ) {
    return frequencer($content);
}

function standardimage($image)
{
    return array(
        '(max-width:400px)' => $image['sizes']['standardphotothumbnail'],
        '(min-width:401px)' => $image['sizes']['standardphoto']
    );
}

function outputimage($image){
    if(is_array($image)) {
        return standardimage($image);
    } else {
        //$placeholder = ...
        //$imagepath = ...
        //if(!file_exists($imagepath)){ $image = $placeholder; }
        return array(
            '(max-width:400px)' => $image,
            '(min-width:401px)' => $image
        );
    }
}

function extractterm($id, $type)
{
    $term = get_the_terms($id, $type);
    return $term[0]->name;
}

function wp_get_attachment_image_url_with_fallback($id, $size)
{
    $imagesizes = get_image_sizes();
    $outputimage = wp_get_attachment_image_url($id, $size);
    if ($outputimage == '') {
        $outputimage ='http://via.placeholder.com/' . $imagesizes[$size]['width'] . 'x' . $imagesizes[$size]['height'];
    }
    return $outputimage;
}

function frequencer($string)
{
    $replacethis = array('Frequencer®');
    $replacedwith = array('Frequencer<sup>&reg;</sup>');
    return str_replace($replacethis,$replacedwith,$string);
}

function barbanamespace($template){
    $template = str_replace(array('.php','page-'),array('',''),$template);
    $namespace = 'common';
    if($template == 'newsroom'){ $namespace = 'newsroom'; }
    if($template == 'contact'){ $namespace = 'contact'; }
    return $namespace;
}

function custom_query_vars_filter($vars) {
    $vars[] = 'selectform';
    $vars[] .= 'formid';
    return $vars;
}
add_filter( 'query_vars', 'custom_query_vars_filter' );





add_action( 'init', function(){
    return add_rewrite_rule(
        'contact/request-a-trial/patient-and-caregiver',
        'index.php?pagename=contact&formid=general-inquiry&selectform=patient_and_caregiver',
        'sitemap_index\.xml$'
    );
});
add_action( 'init', function(){
    return add_rewrite_rule(
        'contact/request-a-trial/healthcare-professional',
        'index.php?pagename=contact&formid=general-inquiry&selectform=healthcare_professional',
        'sitemap_index\.xml$'
    );
});
add_action( 'init', function(){
    return add_rewrite_rule(
        'contact/request-a-trial/healthcare-institution',
        'index.php?pagename=contact&formid=general-inquiry&selectform=healthcare_institution',
        'sitemap_index\.xml$'
    );
});

add_action( 'init', function(){
    return add_rewrite_rule(
        'contact/general-inquiry',
        'index.php?pagename=contact&formid=general-inquiry&selectform=patient_and_caregiver',
        'sitemap_index\.xml$'
    );
});

add_action( 'init', function(){
    return add_rewrite_rule(
        'contact/request-a-trial',
        'index.php?pagename=contact&formid=request-a-trial',
        'sitemap_index\.xml$'
    );
});

add_action( 'init', function(){
    return add_rewrite_rule(
        'contact/order-for-disposables',
        'index.php?pagename=contact&formid=order-for-disposables',
        'sitemap_index\.xml$'
    );
});

add_action( 'init', function(){
    return add_rewrite_rule(
        'contact/order-a-frequencer',
        'index.php?pagename=contact&formid=order-a-frequencer',
        'sitemap_index\.xml$'
    );
});


add_action( 'init', function(){
    return add_rewrite_rule(
        '(fr)/contactez-nous/demande-generale',
        'index.php?pagename=contactez-nous&formid=demande-generale&lang=fr',
        'top'
    );
});
add_action( 'init', function(){
    return add_rewrite_rule(
        '(fr)/contactez-nous/demander-essai',
        'index.php?pagename=contactez-nous&formid=demander-essai&lang=fr',
        'top'
    );
});
add_action( 'init', function(){
    return add_rewrite_rule(
        '(fr)/contactez-nous/commande-jetable',
        'index.php?pagename=contactez-nous&formid=commande-jetable&lang=fr',
        'top'
    );
});
add_action( 'init', function(){
    return add_rewrite_rule(
        '(fr)/contactez-nous/commande-frequencer',
        'index.php?pagename=contactez-nous&formid=commande-frequencer&lang=fr',
        'top'
    );
});
