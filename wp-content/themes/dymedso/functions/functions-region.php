<?php

function add_region_to_url( $rules ) {

    $new_rules = [];

    $region = '(?:us/|international/)?';

    // home page
    $new_rules[$region . '/?$'] = 'index.php';



    // Change other rules
    foreach ( $rules as $key => $rule ) {

        if ( substr( $key, 0, 1 ) === '^' ) {

            $new_rules[ $region . substr( $key, 1 ) ] = $rule;

        } else {

            $new_rules[ $region . $key ] = $rule;

        }
    }

    return $new_rules;
}
//add_filter( 'rewrite_rules_array', 'add_region_to_url' );

function custom_rewrite_tag() {
    add_rewrite_tag('%region%', '([^&]+)');
}
add_action('init', 'custom_rewrite_tag', 10, 0);

function trueid($id){
    $template = get_page_template_slug($id);
    if($template=='page-region.php'){
        $getid = array('fr'=>'region_page_fr','en'=>'region_page_en');
        $return = get_field($getid[pll_current_language('slug')],$id);
    } else {
        $return = $id;
    }
    return $return;
}


function updatebodyclass($classes, $id)
{
    $truetemplate = str_replace(array('page-','.php'),array('',''),get_page_template_slug($id));
    $replaced = str_replace(array(
        'page-template-page-region',
        'page-template-page-region-php'
    ), array(
        'page-template-page-'.$truetemplate,
        'page-template-page-'.$truetemplate.'-php'
    ), implode(' ',$classes));
    return ' class="'.$replaced.'" ';
}
