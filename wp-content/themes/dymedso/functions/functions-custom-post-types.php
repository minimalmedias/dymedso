<?php
add_action('init', 'cpt_testimonials');
add_action('init', 'cpt_reports');
add_action('init', 'cpt_faq');

function cpt_testimonials()
{
    $labels = array(
        'name' => _x('Testimonials', 'post type general name', 'source'),
        'singular_name' => _x('Testimonial', 'post type singular name', 'source'),
        'add_new' => _x('Ajouter', 'book'),
        'add_new_item' => __('Ajouter un Testimonial'),
        'edit_item' => __('Modifier cette fiche de Testimonial'),
        'new_item' => __('Nouveau'),
        'all_items' => __('Tous les Testimonials'),
        'view_item' => __('Afficher la fiche de Testimonial'),
        'search_items' => __('Chercher parmis les Testimonials'),
        'not_found' => __('Aucun Testimonial trouvée'),
        'not_found_in_trash' => __('Aucun Testimonial trouvée dans la corbeille'),
        'parent_item_colon' => '',
        'menu_name' => 'Testimonials'
    );
    $args = array(
        'labels' => $labels,
        'description' => 'Testimonials',
        'public' => true,
        'menu_position' => 5,
        'supports' => array('title', 'editor', 'thumbnail', 'excerpt', 'comments'),
        'has_archive' => true,
        'menu_icon' => 'dashicons-thumbs-up',
    );
    register_post_type('Testimonials', $args);
}

function cpt_reports()
{
    $labels = array(
        'name' => _x('Reports', 'post type general name', 'source'),
        'singular_name' => _x('Report', 'post type singular name', 'source'),
        'add_new' => _x('Ajouter', 'book'),
        'add_new_item' => __('Ajouter un Report'),
        'edit_item' => __('Modifier cette fiche de Report'),
        'new_item' => __('Nouveau'),
        'all_items' => __('Tous les Reports'),
        'view_item' => __('Afficher la fiche de Report'),
        'search_items' => __('Chercher parmis les Reports'),
        'not_found' => __('Aucun Report trouvée'),
        'not_found_in_trash' => __('Aucun Report trouvée dans la corbeille'),
        'parent_item_colon' => '',
        'menu_name' => 'Reports'
    );
    $args = array(
        'labels' => $labels,
        'description' => 'Reports',
        'public' => true,
        'menu_position' => 5,
        'supports' => array('title', 'editor', 'thumbnail', 'excerpt', 'comments'),
        'has_archive' => true,
        'menu_icon' => 'dashicons-chart-pie',
    );
    register_post_type('Reports', $args);
}

function cpt_faq()
{
    $labels = array(
        'name' => _x('FAQ', 'post type general name', 'source'),
        'singular_name' => _x('FAQ', 'post type singular name', 'source'),
        'add_new' => _x('Ajouter', 'book'),
        'add_new_item' => __('Ajouter un FAQ'),
        'edit_item' => __('Modifier cette fiche de FAQ'),
        'new_item' => __('Nouveau'),
        'all_items' => __('Tous les FAQ'),
        'view_item' => __('Afficher la fiche de FAQ'),
        'search_items' => __('Chercher parmis les FAQ'),
        'not_found' => __('Aucun FAQ trouvée'),
        'not_found_in_trash' => __('Aucun FAQ trouvée dans la corbeille'),
        'parent_item_colon' => '',
        'menu_name' => 'FAQ'
    );
    $args = array(
        'labels' => $labels,
        'description' => 'FAQ',
        'public' => true,
        'menu_position' => 5,
        'supports' => array('title', 'editor', 'thumbnail', 'excerpt', 'comments'),
        'has_archive' => true,
        'menu_icon' => 'dashicons-lightbulb',
    );
    register_post_type('FAQ', $args);
}

add_filter('add_meta_boxes', 'hide_meta_boxes');
function hide_meta_boxes() {
    remove_meta_box('wpseo_meta', 'Testimonials', 'normal');
    remove_meta_box('wpseo_meta', 'Reports', 'normal');
    remove_meta_box('wpseo_meta', 'FAQ', 'normal');
}

