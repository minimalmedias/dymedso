<?php if (have_posts()): while (have_posts()): the_post(); ?>
    <?php get_header(); ?>
    <section class="headerspaced bg-paleblue py-5">
        <div class="container my-lg-5">
            <div class="row">
                <div class="col text-center py-lg-5">
                    <h1><?php the_title() ?></h1>
                </div>
            </div>
        </div>
    </section>
    <section class="py-5 mt-lg-5">
        <div class="container my-lg-5">
            <div class="row justify-content-center">
                <div class="col-md-9 text-center text-md-left">
                    <?php the_content() ?>
                </div>
            </div>
        </div>
    </section>
    <?php get_footer(); ?>
<?php endwhile; else: ?>
    <?php wp_redirect(get_bloginfo('url') . '/404', 404); ?>
<?php endif; ?>

