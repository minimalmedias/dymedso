<?php
function set_heading($return)
{
    $heading['class'] = 'col-md-auto';
    $heading['background'] = get_stylesheet_directory_uri() . '/assets/img/patients_and_caregivers/heading.jpg';
    $heading['text'] = '<div class="text-right mr-0 patients-heading">';
    $heading['text'] .= '<h2>' . pll__('"Since my son is using the Frequencer® , his health and independence increased significantly."') . '</h2>';
    $heading['text'] .= '<p class="text-right">' . pll__('-From a father') . '</p>';
    $heading['text'] .= '</div>';
    $heading['align'] = 'justify-content-end';
    return $heading[$return];
}