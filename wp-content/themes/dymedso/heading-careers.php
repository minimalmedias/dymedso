<?php
function set_heading($return)
{
    $heading['class'] = 'col-auto';
    $heading['background'] = get_stylesheet_directory_uri() . '/assets/img/headingcareers.jpg';
    $heading['text'] = '<div class="text-center m-auto career-heading">';
    $heading['text'] .= '<h1>' . pll__('Help us help others.');
    $heading['text'] .= '<br>';
    $heading['text'] .= pll__('Join our team.') . '</h1>';
    $heading['text'] .= '</div>';
    $heading['align'] = 'justify-content-center';
    return $heading[$return];
}