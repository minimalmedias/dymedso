<?php
function set_heading($return)
{
    $heading['class'] = 'col-auto';
    $heading['background'] = get_stylesheet_directory_uri() . '/assets/img/healthcare_institutions/heading.jpg';
    $heading['text'] = '<div class="text-left m-auto institution-heading">';
    $heading['text'] .= '<h1>' . pll__('Investing in better patient outcomes') . '</h1>';
    $heading['text'] .= '<img src="' . get_stylesheet_directory_uri() . '/assets/img/logo/frequencer_logo_' . pll_current_language('slug') . '.png" alt="Frequencer" class="frequencerlogo">';
    $heading['text'] .= '</div>';
    $heading['align'] = 'justify-content-end';
    return $heading[$return];
}