<?php /** Template Name: Newsroom */
global $post;
$postID =  $post->ID;
get_header(); ?>
<section id="newsroom" class="headerspaced headersized bg-paleblue py-5">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-8 text-center">
                <h2 class="mt-5 mt-lg-0 mb-md-4"><?php echo pll__('Newsroom'); ?></h2>
                <h3 class="mb-5 pb-5"><?php echo pll__('Finding new ways to make a difference in the life of patients around the world'); ?></h3>
            </div>
        </div>
        <div class="row justify-content-center" id="articleloader" style="opacity:0;"></div>
    </div>
</section>
<?php get_footer(); ?>
