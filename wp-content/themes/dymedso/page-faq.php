<?php /** Template Name: FAQ */
get_header();
?>
<section id="faq" class="headerspaced bg-paleblue py-5">
    <div class="container my-md-5">
        <div class="row">
            <div class="col text-center pb-5 py-md-5">
                <h1><?php echo pll__('FAQ'); ?></h1>
            </div>
        </div>
    </div>
    <?php
    echo responsiveimage(array(
        'url' => outputimage(get_stylesheet_directory_uri() . '/assets/img/patients_and_caregivers/Image_machine_vue_2.png'),
        'alt' => pll__('Design to give you autonomy'),
        'classes' => 'img-fluid underedge'
    ))
    ?>
</section>
<section class="py-5 mt-5">
    <div class="container my-5">
        <div class="row">
            <div class="col-md-12 collapsemenu" id="faq-accordion" role="tablist">
                <?php
                $i = 0;
                $items=array();
                $wp_query = new WP_Query(array('post_status' => 'publish','post_type'=>'FAQ','posts_per_page'=>'-1'));
                if ($wp_query->have_posts()):
                    while ($wp_query->have_posts()) : $wp_query->the_post();
                        $id = get_the_ID();
                        $items[$id]=array(
                            frequencer(get_the_title()),
                            frequencer(get_field('field_5a68dca67ab7f'))
                        );
                    endwhile;
                endif;
                foreach ($items as $row):
                    ?>
                    <div class="collapsable card">
                        <div class="card-header" role="tab" id="heading<?php echo $i; ?>">
                            <h5 class="mb-0">
                                <a
                                    <?php echo ($i != 0) ? 'class="collapsed"' : ''; ?>
                                        data-toggle="collapse"
                                        href="#collapse<?php echo $i; ?>"
                                        aria-expanded="true"
                                        aria-controls="collapse<?php echo $i; ?>">
                                    <span class="title"><?php echo $row[0]; ?></span>
                                </a>
                            </h5>
                        </div>
                        <div id="collapse<?php echo $i; ?>" class="collapse  <?php echo ($i == 0) ? 'show' : ''; ?>"
                             role="tabpanel" aria-labelledby="heading<?php echo $i; ?>"
                             data-parent="#faq-accordion">
                            <div class="card-body">
                                <?php echo $row[1]; ?>
                            </div>
                        </div>
                    </div>
                    <?php
                    $i++;
                endforeach;
                ?>
            </div>
        </div>
    </div>
</section>
<?php get_footer(); ?>
