<?php /** Template Name: Healthcare Institutions */
global $post;
$postID =  $post->ID;
$blocs = array();
$b=0;
while (have_rows('field_5a6a01b1297b0',trueid($postID))) {
    the_row();
    if (get_sub_field('') != 'field_5a6a0cb97e11f'){ $blocs[$b]['id'] = get_sub_field('field_5a6a0cb97e11f'); }
    if (get_sub_field('field_5a6a01c4297b1') != ''){ $blocs[$b]['headertitle'] = get_sub_field('field_5a6a01c4297b1'); }
    while (have_rows('field_5a6a01d6297b2')) {the_row();$blocs[$b]['list'][] = get_sub_field('field_5a6a021f297b3');}
    if (get_sub_field('field_5a6a0287297b5') != ''){ $blocs[$b]['button_label'] = get_sub_field('field_5a6a0287297b5'); }
    if (get_sub_field('') != 'field_5a6a02c3297b6'){ $blocs[$b]['button_link'] = get_sub_field('field_5a6a02c3297b6'); }
    if (get_sub_field('field_5a6a023c297b4') != ''){ $blocs[$b]['image'] = get_sub_field('field_5a6a023c297b4'); }
    $b++;
}

foreach($blocs as $b){
    $nav[$b['id']]=$b['headertitle'];
}
$nav[pll__('patient-satisfaction')] = pll__('Patient Satisfaction');
$nav[pll__('testimonials')] = pll__('Testimonials');


get_header(); ?>

<div class="mt-md-5"><?php minimal_get_template_part('/templates/pagenav.php', array('nav' => $nav)); ?></div>

<?php
$countblocs = count($blocs);
$c = 0;
foreach ($blocs as $im):
    echo '<section class="' . ($c != $countblocs ? 'mb-5' : '') . ' mb-md-0 mt-md-5 pt-md-5" id="' .  $im['id'] . '">';
    minimal_get_template_part('/templates/image-texte.php', array(
        'image' => responsiveimage(array(
            'url' => outputimage($im['image']),
            'alt' => $im['headertitle'],
            'classes' => 'img-fluid'
        )),
        'text' => array(
            'headertitle' => $im['headertitle'],
            'list' => $im['list'],
            'button2' => array(
                'label' => array_key_exists('button_label', $im) ? $im['button_label'] : '',
                'url' => array_key_exists('button_link', $im) ? $im['button_link'] : '',
            )
        ),
        'layout' => (++$c % 2) ? 'text2-image' : 'image-text'
    ));
    echo '</section>';
    $c++;
endforeach;
?>
<section class="chartbg py-5" id="<?php echo pll__('patient-satisfaction'); ?>">
    <div class="container my-md-4 layout-image-text reveal revealed">
        <div class="row">
            <div class="col-12 text-center">
                <h3 class="h2 centered mt-5"><?php echo pll__('Patient Satisfaction'); ?></h3>
            </div>
        </div>
        <div class="row my-5">
            <?php foreach (array(
                               1 => pll__('Gentle treatment very well tolerated by frail patients'),
                               2 => pll__('Allows for multiple treatments per day can shorten duration of stay and helps patient get better faster'),
                               3 => pll__('Ideal for patient facing challenging conditions (G-tube, trauma, obese...)'),
                               4 => pll__('Non-disruptive therapy, only emits 65 Db (sound level of a background music)')
                           ) as $key => $col): ?>
                <div class="col-md-3">
                    <p class="numbertitle"><?php echo $key; ?></p>
                    <p class="chartcol"><?php echo $col; ?></p>
                </div>
            <?php endforeach; ?>
        </div>
        <div class="row justify-content-center">
            <div class="col-lg-5 text-center">
                <div class="p-lg-5 m-auto">
                    <?php echo responsiveimage(array(
                        'url' => outputimage(get_stylesheet_directory_uri() . '/assets/img/healthcare_institutions/convenience.png'),
                        'alt' => pll__(''),
                        'classes' => 'img-fluid'
                    )) ?>
                </div>
            </div>
            <div class="col-lg-5 text-center">
                <div class="p-lg-5 m-auto">
                    <?php echo responsiveimage(array(
                        'url' => outputimage(get_stylesheet_directory_uri() . '/assets/img/healthcare_institutions/Demographics.png'),
                        'alt' => pll__(''),
                        'classes' => 'img-fluid'
                    )) ?>
                </div>
            </div>
        </div>
        <div class="row justify-content-center mt-5 mt-md-0">
            <div class="col-md-8 text-center">
                <p class="medium"><?php echo pll__('Using a Baylor University standardised questionnaire, patients assess their currently used clearance technique versus the Frequencer<sup>&reg;</sup> over a 2-week trial period.'); ?></p>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-8 text-center">
                <p class="p-md-5 mt-5"><a href="<?php echo get_template_link('page-contact.php'); ?>#contactform" class="btn btn-primary"><?php echo pll__('Request more information'); ?></a></p>
            </div>
        </div>
    </div>
</section>
<?php minimal_get_template_part('/templates/testimonials.php', array('classes' => '')); ?>
<?php get_footer(); ?>
