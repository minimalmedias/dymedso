<?php
/*
The Search Loop
===============
*/
?>

<?php

//filter by parent
$parent = 0;
if (isset($_COOKIE["dymedsoregion"])) {
    $parents['canada']['fr'] = 0;
    $parents['canada']['en'] = 0;
    $parents['international']['fr'] = 517;
    $parents['international']['en'] = 519;
    $parent = $parents[$_COOKIE["dymedsoregion"][pll_current_language('slug')]];
}

if (have_posts()): while (have_posts()): the_post(); ?>
    <?php //if (wp_get_post_parent_id(get_the_ID()) == $parent): ?>
        <article role="article" id="post_<?php the_ID() ?>" <?php post_class() ?>>
            <header>
                <h4><a href="<?php the_permalink(); ?>" class="regionset"><?php the_title() ?></a></h4>
            </header>
            <?php the_excerpt(); ?>
        </article>
    <?php //endif; ?>
<?php endwhile; else: ?>
    <div class="alert alert-warning">
        <i class="fa fa-exclamation-triangle"></i> <?php _e('Sorry, your search yielded no results.', 'b4st'); ?>
    </div>
<?php endif; ?>
