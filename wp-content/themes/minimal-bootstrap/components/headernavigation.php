<?php
/*
COMPONENT: HEADER NAVIGATION
    minimal_get_template_part('/components/headernavigation.php',array(
        'homeurl' => string         // Url for logo link
        'logo' => string,           // text/image/object for logo
        'id' => string,             // id for nav container
        'classes' => string,        // optional classes for nav container
        'headerextra' => html,      // optional content for header, typicaly aligned right and mobile only. ie: Language
        'menuobject' => obj|html,   // typicaly wp_nav_menu() object(s)
        'fullwidth' => true|false   // if set and set to true, header nav is 100% of its container. Otherwise a container div added.
    ));
*/
$nav_id = 'navbarResponsive';
?>
<nav class="<?php echo (isset($this->classes))?$this->classes:'navbar'; ?>" <?php echo (isset($this->id))?' id="'.$this->id.'"':''; ?>>
    <?php echo (isset($this->fullwidth) and $this->fullwidth == true)?'':'<div class="container">'; ?>
    <button class="navbar-toggler collapsed" type="button" data-toggle="collapse"
            data-target="#<?php echo $nav_id; ?>"
            aria-controls="<?php echo $nav_id; ?>" aria-expanded="false" aria-label="Toggle navigation">
        <span class="hamburger"></span>
    </button>
    <a class="navbar-brand" href="<?php echo $this->homeurl; ?>"><?php echo $this->logo; ?></a>
    <div class="collapse navbar-collapse" id="<?php echo $nav_id; ?>">
        <?php
        if(is_array($this->menuobject)){
            foreach($this->menuobject as $menu){
                echo $menu;
            }
        }else{
            echo $this->menuobject;
        }
        ?>
    </div>
    <?php echo (isset($this->headerextra))?$this->headerextra:''; ?>
    <?php echo (isset($this->fullwidth) and $this->fullwidth == true)?'':'</div>'; ?>
</nav>