<?php
function ajax_loader(
    $loaderTag, // '<div style="width:100%; text-align: center"><div class="loader lds-css ng-scope" style="margin:auto; width:100px;"><div style="width:100%;height:100%" class="lds-dual-ring"><div></div></div></div></div>';
    $loadAreaId, //'#articleloader';
    $filterClass, //'.inspirationstypes a';
    $items_per_load, //2;
    $template, //'/templates/ajaxloop.php'; copy this template file and change for your needs
    $showposts, // 9999;
    $filter_by // json '[{"category_name":"general","tax_query":{"taxonomy":"pays","field":"slug","terms":{"canada"}}}]' array of filters and default values for initial loading
    //see https://developer.wordpress.org/reference/classes/wp_query/parse_query/ for list of available arguments
)
{ ?>
    <script>
        jQuery(document).ready(function () {
            var loaderTag = <? echo $loaderTag; ?>;
            var loadAreaId = <? echo $loadAreaId; ?>;
            var filterClass = <? echo $filterClass; ?>;
            var items_per_load = <? echo $items_per_load; ?>;
            var template = <? echo $template; ?>;
            var showposts = <? echo $showposts; ?>;
            var filter_by = <? echo $filter_by; ?>;
        })
    </script>
<?php }

add_action('wp_footer', 'footer_script');

function filtermenuarray(
    $postfilters, // 'get_categories'|'get_terms'
    $postfiltersoptions, // array('taxonomy'=>'pagetype','parent'=>0)|array('taxonomy'=>'pays','parent'=>0)
    $formatting //'<a href="#" {{filter}} class="btn">{{label}}</a>'
)
{
    $links = array();
    $filters = $postfilters($postfiltersoptions);
    if (is_array($filters)) {
        foreach ($filters as $filter) {
            if($postfilters=='get_categories') {
                $data = '[{"category_name":"' . $filter->category_name . '"}]';
            }
            if($postfilters=='get_terms') {
                $data = '[{"tax_query":{{"taxonomy":"' . $postfiltersoptions['taxonomy'] . '","field":"slug","terms":"' . $filter->slug . '"}}}]';
            }
            $links[] = str_replace(
                    array('{{filter}}','{{label}}'),
                    array('data-filter="'.$data.'"',$filter->name),
                    $filter);
        }
    }
    return $links;
}