<?php
function my_acf_add_local_field_groups()
{

    acf_add_local_field_group(array(
        'key' => 'group_1',
        'title' => 'My Group',
        'fields' => array(
            array(
                'key' => 'field_1',
                'label' => 'Sub Title',
                'name' => 'sub_title',
                'type' => 'text',
            )
        ),
        'location' => array(
            array(
                array(
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => 'post',
                ),
            ),
        ),
    ));

}

add_action('acf/init', 'my_acf_add_local_field_groups');

function addfields($location, $functions)
{
    $i = 0;
    foreach ($functions as $function) {
        acf_add_local_field_group($function[0]($function[1], $function[2], $location, $i));
        $i++;
    }
}

function repeater($id, $options, $location, $position)
{
    //$options = array('title',array(
    //array('subid01','title','text')
    //));
    $sub_fields = array();
    foreach($options[1] as $sub_field){
        $sub_fields[] = subfield($id,$sub_field[0],$sub_field[1],$sub_field[2]);
    }
    return array(
        'key' => $id,
        'title' => $options[0],
        'fields' => array(
            array(
                'sub_fields' => $sub_fields,
                'min' => 0,
                'max' => 0,
                'layout' => 'table',
                'button_label' => '',
                'collapsed' => '',
                'key' => $id . 'field_5a09e0c2e38c9',
                'label' => 'repeaterbox',
                'name' => 'repeaterbox',
                'type' => 'repeater',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
            ),
        ),
        'location' => array(
            array(
                array(
                    'param' => 'page_template',
                    'operator' => '==',
                    'value' => $location,
                ),
            ),
        ),
        'menu_order' => $position,
        'position' => 'acf_after_title',
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => array (
            0 => 'the_content',
        ),
        'active' => 1,
        'description' => '',
    );

}

function textimage($id, $title, $location, $position)
{
    return array(
        'id' => $id,
        'title' => $title,
        'fields' => array(
            array(
                'key' => $id . '_field_59e4eca257ba1',
                'label' => 'Title',
                'name' => $id . '_title',
                'type' => 'text',
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'formatting' => 'html',
                'maxlength' => '',
            ),
            array(
                'key' => $id . '_field_59e4ecc957ba2',
                'label' => 'Text',
                'name' => $id . '_text',
                'type' => 'wysiwyg',
                'default_value' => '',
                'toolbar' => 'basic',
                'media_upload' => 'no',
            ),
            array(
                'key' => $id . '_field_59e4ece457ba3',
                'label' => 'Button Label',
                'name' => $id . '_button_label',
                'type' => 'text',
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'formatting' => 'html',
                'maxlength' => '',
            ),
            array(
                'key' => $id . '_field_59e4ed4557ba4',
                'label' => 'Button link',
                'name' => $id . '_button_link',
                'type' => 'url',
                'post_type' => array(
                    0 => 'all',
                ),
                'allow_null' => 0,
                'multiple' => 0,
            ),
            array(
                'key' => $id . '_field_59e4ed8b5c658',
                'label' => 'Image',
                'name' => $id . '_image',
                'type' => 'image',
                //'required' => 1,
                'save_format' => 'object',
                'preview_size' => 'thumbnail',
                'library' => 'all',
            ),
            array(
                'key' => $id . '_field_59e4ee945db1f',
                'label' => 'Image Label',
                'name' => $id . '_image_label',
                'type' => 'text',
                //'required' => 1,
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'formatting' => 'html',
                'maxlength' => '',
            ),
            /*
            array(
                'key' => $id . '_field_59e4eeaa5db20',
                'label' => 'Vertical Label',
                'name' => $id . '_vertical_label',
                'type' => 'text',
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'formatting' => 'html',
                'maxlength' => '',
            )
            */
        ),
        'location' => array(array(array(
            'param' => 'page_template',
            'operator' => '==',
            'value' => $location,
            'order_no' => $position,
            'group_no' => $position,
        ))),
        'options' => array(
            'position' => 'acf_after_title',
            'layout' => 'default',
            'hide_on_screen' => array (
                0 => 'the_content',
            ),
        ),
        'menu_order' => $position,
    );
}

function blurb($id, $title, $location, $position)
{
    return array(
        'id' => $id . '_blurb',
        'title' => 'Blurb',
        'fields' => array(
            array(
                'key' => $id . '_blurbfield_59e652e8178dd',
                'label' => $title,
                'name' => $id . '_blurb',
                'type' => 'text',
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'formatting' => 'html',
                'maxlength' => '',
            ),
        ),
        'location' => array(array(array(
            'param' => 'page_template',
            'operator' => '==',
            'value' => $location,
            'order_no' => $position,
            'group_no' => $position,
        ))),
        'options' => array(
            'position' => 'acf_after_title',
            'layout' => 'default',
            'hide_on_screen' => array (
                0 => 'the_content',
            ),
        ),
        'menu_order' => $position,
    );
}

function button($id, $title, $location, $position)
{
    return array(
        'id' => $id . '_button',
        'title' => $title,
        'fields' => array(
            array(
                'key' => $id . '_field_59e4ece457ba3',
                'label' => 'Button Label',
                'name' => $id . '_button_label',
                'type' => 'text',
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'formatting' => 'html',
                'maxlength' => '',
            ),
            array(
                'key' => $id . '_field_59e4ed4557ba4',
                'label' => 'Button link',
                'name' => $id . '_button_link',
                'type' => 'url',
                'post_type' => array(
                    0 => 'all',
                ),
                'allow_null' => 0,
                'multiple' => 0,
            ),
        ),
        'location' => array(array(array(
            'param' => 'page_template',
            'operator' => '==',
            'value' => $location,
            'order_no' => $position,
            'group_no' => $position,
        ))),
        'options' => array(
            'position' => 'acf_after_title',
            'layout' => 'default',
            'hide_on_screen' => array (
                0 => 'the_content',
            ),
        ),
        'menu_order' => $position,
    );
}

function acfimage($id, $title, $location, $position)
{
    return array(
        'id' => $id,
        'title' => $title,
        'fields' => array(
            array(
                'key' => $id . '_field_59e4ed8b5c658acfimage',
                'label' => 'Image',
                'name' => $id . '_image',
                'type' => 'image',
                //'required' => 1,
                'save_format' => 'object',
                'preview_size' => 'thumbnail',
                'library' => 'all',
            ),
            array(
                'key' => $id . '_field_59e4ee945db1facfimage',
                'label' => 'Image Label',
                'name' => $id . '_image_label',
                'type' => 'text',
                //'required' => 1,
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'formatting' => 'html',
                'maxlength' => '',
            )
        ),
        'location' => array(array(array(
            'param' => 'page_template',
            'operator' => '==',
            'value' => $location,
            'order_no' => $position,
            'group_no' => $position,
        ))),
        'options' => array(
            'position' => 'acf_after_title',
            'layout' => 'default',
            'hide_on_screen' => array (
                0 => 'the_content',
            ),
        ),
        'menu_order' => $position,
    );
}

function textarea($id, $title, $location, $position)
{
    return array(
        'id' => $id . '_blurb',
        'title' => $title,
        'fields' => array(
            array(
                'default_value' => '',
                'new_lines' => 'wpautop',
                'maxlength' => '',
                'placeholder' => '',
                'rows' => '',
                'key' => $id . '_field_59e6601efadbe',
                'label' => 'Text',
                'name' => $id . '_textarea',
                'type' => 'wysiwyg',
                'default_value' => '',
                'toolbar' => 'basic',
                'media_upload' => 'no',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
            )
        ),
        'location' => array(array(array(
            'param' => 'page_template',
            'operator' => '==',
            'value' => $location,
            'order_no' => $position,
            'group_no' => $position,
        ))),
        'options' => array(
            'position' => 'acf_after_title',
            'layout' => 'default',
            'hide_on_screen' => array (
                0 => 'the_content',
            ),
        ),
        'menu_order' => $position,
    );
}

function collapsebox($id, $titlearray, $location, $position)
{
    $title = $titlearray[0];
    $label = $titlearray[1];
    $description = $titlearray[2];

    return array(
        'key' => $id . '_group_59e667fd6de75',
        'title' => $title,
        'fields' => array(
            array(
                'sub_fields' => array(
                    array(
                        'default_value' => '',
                        'maxlength' => '',
                        'placeholder' => '',
                        'prepend' => '',
                        'append' => '',
                        'key' => $id . '_field_59e6681cb9199',
                        'label' => $label,
                        'name' => $id . '_label',
                        'type' => 'text',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                    ),
                    array(
                        'tabs' => 'all',
                        'toolbar' => 'basic',
                        'media_upload' => 0,
                        'default_value' => '',
                        'delay' => 0,
                        'key' => $id . '_field_59e6682eb919a',
                        'label' => $description,
                        'name' => $id . '_description',
                        'type' => 'wysiwyg',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                    ),
                ),
                'min' => 0,
                'max' => 0,
                'layout' => 'table',
                'button_label' => '',
                'collapsed' => '',
                'key' => $id . '_field_59e66806b9198',
                'label' => 'Collapse Box',
                'name' => $id,
                'type' => 'repeater',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
            ),
        ),
        'location' => array(array(array(
            'param' => 'page_template',
            'operator' => '==',
            'value' => $location,
            'order_no' => $position,
            'group_no' => $position,
        ))),
        'menu_order' => $position,
        'position' => 'acf_after_title',
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => array (
            0 => 'the_content',
        ),
        'active' => 1,
        'description' => '',
    );
}

function subfield($id,$subid,$title,$type)
{
    $return['text'] = array(
        'default_value' => '',
        'maxlength' => '',
        'placeholder' => '',
        'prepend' => '',
        'append' => '',
        'key' => $id.'_'.$subid,
        'label' => $title,
        'name' => $id.'_'.$subid,
        'type' => 'text',
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => 0,
        'wrapper' => array(
            'width' => '',
            'class' => '',
            'id' => '',
        ),
    );
    $return['image'] = array(
        'return_format' => 'url',
        'preview_size' => 'thumbnail',
        'library' => 'all',
        'min_width' => '',
        'min_height' => '',
        'min_size' => '',
        'max_width' => '',
        'max_height' => '',
        'max_size' => '',
        'mime_types' => '',
        'key' => $id.'_'.$subid,
        'label' => $title,
        'name' => $id.'_'.$subid,
        'type' => 'image',
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => 0,
        'wrapper' => array(
            'width' => '',
            'class' => '',
            'id' => '',
        ),
    );
    $return['textarea'] = array(
        'default_value' => '',
        'new_lines' => 'wpautop',
        'maxlength' => '',
        'placeholder' => '',
        'rows' => '',
        'key' => $id.'_'.$subid,
        'label' => $title,
        'name' => $id.'_'.$subid,
        'type' => 'wysiwyg',
        'default_value' => '',
        'toolbar' => 'basic',
        'media_upload' => 'no',
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => 0,
        'wrapper' => array(
            'width' => '',
            'class' => '',
            'id' => '',
        ),
    );
    return $return[$type];
}