<?php
function bootstrap4_ie8()
{
    echo '
    <!--[if IE]>
      <link href="' . get_template_directory_uri() . '/assets/css/bootstrap-ie9.css" rel="stylesheet">
      <script src="https://cdn.jsdelivr.net/g/html5shiv@3.7.3"></script>
    <![endif]-->
    <!--[if lt IE 9]>
      <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	  <link href="' . get_template_directory_uri() . '/assets/css/bootstrap-ie8.css" rel="stylesheet">
    <![endif]-->
    ';
}

add_action('wp_head', 'bootstrap4_ie8');